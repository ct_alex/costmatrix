library(assertthat)

key_value_shell <- list(
  type = "key_value",
  compulsory_fields = c(),
  optional_fields = c(),
  fields = list(),
  constraints = list()
)

list_shell <- list(
  type = "list",
  min_length = 0,
  max_length = 9999,
  values = list(),  # one of key_value_design, list_design, scalar_design
  constraints = list()
)

scalar_shell <- list(
  type = "scalar",
  value_type = "numeric", # "logical", "character", etc.
  constraints = list() # optional, if any (for numeric > 0 for example)
)

verify_next <- function (design, actual) {
  
  if (design$type == "key_value") {
    
    verify_key_value(design, actual)
    
  } else if (design$type == "list") {
    
    verify_list(design, actual)
    
  } else if (design$type == "scalar") {
    
    verify_scalar(design, actual)
  }
}

verify_key_value <- function (design, actual) {
  
  # Type of the object
  assert_that(
    are_equal("list", typeof(actual)),
    msg = paste0("\nType of input <", typeof(actual), "> differs from expected type <list>"))
  
  # Compulsory fields
  b_compulsory <- design$compulsory_fields %in% names(actual)
  assert_that(
    all(b_compulsory), 
    msg = paste0("\nMissing required fields: ", paste0(design$compulsory_fields[!b_compulsory], collapse = ", ")))
  
  # Unknown fields
  unknown_fields <- setdiff(names(actual), c(design$compulsory_fields, design$optional_fields))
  assert_that(
    are_equal(0, length(unknown_fields)), 
    msg = paste0("\nUnknown fields detected: ", paste0(unknown_fields, collapse = ", ")))
  
  # Key and values
  for (field in names(actual)) {
    tryCatch(
      verify_next(design$fields[[field]], actual[[field]]),
      error = function (e) {
        stop(paste0('\nAt field "', field, '":', e$message))
      }
    )
  }
  
  # Constraints
  for (constraint in names(design$constraints)) {
    
    tryCatch(
      constraint_functions[[constraint]](design$constraints[[constraint]], actual),
      error = function (e) {
        stop(paste0('\nConstraint violated: "', constraint, '"\n\t', e$message))
      }
    )
  }
}

verify_list <- function (design, actual) {
  
  # Type of the object
  assert_that(
    are_equal("list", typeof(actual)),
    msg = paste0("\nType of input <", typeof(actual), "> differs from expected type <list>"))
  
  # Minimum length
  assert_that(
    length(actual) >= design$min_length, 
    msg = paste0("\nList length <", length(actual), "> below minimum required length <", design$min_length, ">"))
  
  # Maximum length
  assert_that(
    length(actual) <= design$max_length, 
    msg = paste0("\nList length <", length(actual), "> above maximum allowed length <", design$max_length, ">"))
  
  # Elements
  for (i in 1 : length(actual)) {
    tryCatch(
      verify_next(design$values, actual[[i]]),
      error = function (e) {
        stop(paste0("\nAt element ", i, ":", e$message))
      }
    )
  }
  
  # Constraints
  for (constraint in names(design$constraints)) {
    
    tryCatch(
      constraint_functions[[constraint]](design$constraints[[constraint]], actual),
      error = function (e) {
        stop(paste0('\nConstraint violated: "', constraint, '"\n\t', e$message))
      }
    )
  }
}

verify_scalar <- function (design, actual) {
  
  # Type of the object
  assert_that(
    are_equal(design$value_type, typeof(actual)), 
    msg = paste0("\nType of input <", typeof(actual), "> differs from expected type <", design$value_type, ">"))
  
  # Constraints
  for (constraint in names(design$constraints)) {
    
    tryCatch(
      constraint_functions[[constraint]](design$constraints[[constraint]], actual),
      error = function (e) {
        stop(paste0('\nConstraint violated: "', constraint, '"\n\t', e$message))
      }
    )
  }
}
