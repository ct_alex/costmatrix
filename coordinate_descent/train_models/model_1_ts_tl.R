library(tidyverse)

# Variant
source('~/Alex/coordinate_descent/fx_objective_function_helpers.R')
source('~/Alex/coordinate_descent/model_definitions/model_1.R')

# Invariant
source('~/Alex/coordinate_descent/fx_index_functions.R')
source('~/Alex/coordinate_descent/fx_param_utility.R')
source('~/Alex/coordinate_descent/fx_bayesian_weighting.R')
source('~/Alex/coordinate_descent/fx_coordinate_descent.R')
source('~/Alex/coordinate_descent/fx_learning_rate.R')
source('~/Alex/coordinate_descent/fx_plot.R')
source('~/Alex/coordinate_descent/fx_data.R')

# # Choose a spatial index
# data_map <- read_csv(file.path(data_map_path, region, "region_spatial_index.csv"))
# data_map %>% group_by(spatial_emb_indices) %>% summarise(n = n()) %>% arrange(-n)

# Paths ----
data_map_path <- "data/region_spatial_map"
path_data_dir <- "C:/Users/AI/Documents/Alex/terminal_event_path_data/data/path_training_data"
grand_ref_path <- "C:/Users/AI/Documents/Alex/terminal_event_path_data/data/flat_indices_grand.csv"
link_neighbour_dir <- "C:/Users/AI/Documents/Alex/coordinate_descent/data/region_neighbour_lists"
spatial_neighbour_dir <- "C:/Users/AI/Documents/Alex/coordinate_descent/data/spatial_neighbours"

# Current set ----
region <- "za1"
spatial_index_ <- "653"
save_dir <- file.path("za1_617", "test_ts_tl")

# Neighbour lists ----
temporal_neighbours <- get_default_temporal_neighbours()

spatial_neighbours <- readRDS(file.path(spatial_neighbour_dir, region, "node_neighbour_list.rds"))
spatial_neighbours <- modify_spatial_neighbours(spatial_index_, spatial_neighbours)

grand_df <- read_csv(grand_ref_path, col_types = "ccccccc") %>% 
  rename(region_ = region) %>% 
  filter(region_ == region, spatial_emb_indices %in% names(spatial_neighbours)) %>% 
  transmute(
    lat = lat,
    long = long,
    link_index = region_ref, 
    spatial_index = spatial_emb_indices
  )

# TODO: Links are cut when travelling across spatial indices. Correct this
link_neighbours <- readRDS(file.path(link_neighbour_dir, region, "node_neighbour_list.rds"))
link_neighbours <- modify_link_neighbours(link_neighbours, link_neighbour_set = unique(grand_df$link_index[grand_df$spatial_index == spatial_index_]))

x <- get_data(
  region, names(spatial_neighbours), folder_set = NULL, 
  data_map_path = data_map_path, 
  path_data_dir = path_data_dir,
  grand_ref_path = grand_ref_path)
x <- x %>% filter(p < quantile(x$p, .99))

obj_fx_builder <- objective_function_builder(
  index_neighbours = list(
    temporal = temporal_neighbours,
    spatial = spatial_neighbours,
    link = link_neighbours
  ), 
  z_functions = list(
    temporal = get_univariate_importance_function(get_bayesian_weights(x, "temporal_index")),
    temporal_spatial = get_bivariate_importance_function(get_bayesian_weights(x, "temporal_index", "spatial_index")),
    temporal_link = get_bivariate_importance_function(get_bayesian_weights(
      x %>% filter(link_index %in% names(link_neighbours), spatial_index == spatial_index_), "temporal_index", "link_index"))
  ))

# If we train temporal_spatial first, then link_index does not affect the expected value since it is zero
# So create two complete x's: one for temporal and spatial, one for temporal and link

# Temporal - Spatial objects ----
x_ts <- complete(
  x, 
  temporal_index = names(temporal_neighbours), spatial_index = names(spatial_neighbours),
  fill = list(link_index = "0", p = 1)
)

mse_ts <- get_mse_function(x_ts)

n_levels_ts <- nrow(x_ts %>% select(temporal_index, spatial_index) %>% distinct())
sample_df_ts <- x_ts %>% 
  left_join(
    x_ts %>% 
      group_by(temporal_index, spatial_index) %>% 
      summarise(n = n(), prob = 1 / n / n_levels_ts), 
    by = c("temporal_index", "spatial_index")) %>% 
  select(prob) %>% 
  mutate(rn = row_number())

# Temporal - Link objects ----

# Since the number of combinations are #temporal_index * #link_index <= 2016 * 1000 ~= 2000000, we need to be a little more careful
# Complete the x matrix by only considering the temporal and link neighbours of each observation point
# This is sufficient since the regularisers only consider immediate neighbours

# This may not be a good idea if the values of tl are conditional on ts since the "missed" values assume ts params
# Otherwise, condition the expectation of y_hat as 
# y_hat = 1 + I(t,l)*ts(t,s) + tl(t,l) where I(t,l) == 1 if (t,l) is part of the training sample
future::plan(multisession)
x_distinct <- x %>% filter(spatial_index == spatial_index_, link_index %in% names(link_neighbours)) %>% select(-p, -spatial_index) %>% distinct()
n_xd <- nrow(x_distinct)
x_tl <- furrr::future_map_dfr(1 : nrow(x_distinct), function (i, temporal_indices, link_indices) {
  
  if (round(i/n_xd * 1000) %% 100 == 0) print(i / n_xd)
  
  return (data.frame(
    temporal_index = temporal_indices[i], link_index = link_indices[i]
  ) %>% 
    complete(
      temporal_index = temporal_neighbours[[temporal_indices[i]]],
      link_index = link_neighbours[[link_indices[i]]]
    ))
  
}, temporal_indices = x_distinct$temporal_index, link_indices = x_distinct$link_index) %>% 
  drop_na() %>% 
  distinct() %>% 
  mutate(spatial_index = spatial_index_)

x_distinct <- NULL
x_tl <- setdiff(x_tl, x %>% select(-p))
x_tl <- rbind(
  x_tl %>% mutate(p = 1), 
  x %>% filter(
    link_index %in% names(link_neighbours), 
    spatial_index == spatial_index_))

mse_tl <- get_mse_function(x %>% filter(spatial_index == spatial_index_, link_index %in% names(link_neighbours)))

n_levels_tl <- nrow(x_tl %>% select(temporal_index, link_index) %>% distinct())
sample_df_tl <- x_tl %>% 
  left_join(
    x_tl %>% 
      group_by(temporal_index, link_index) %>% 
      summarise(n = n(), prob = 1 / n / n_levels_tl), 
    by = c("temporal_index", "link_index")) %>% 
  select(prob) %>% 
  mutate(rn = row_number())

# Train ----

regulariser_strengths <- c(0.0001, 0.001, 0.01, 0.1, 1, 2, 5, 10)

# Train temporal spatial parameters first (ts)
for (a in 1 : length(regulariser_strengths))  {
  
  if (a != 1) {
    
    # Use previous parameters as starting point
    pl <- readRDS(file.path(save_dir, paste0(regulariser_strengths[a-1], ".rds")))
    # new params function changes!
    train(
      x_ts, obj_fx_builder, mse_ts, 
      params = list(
        temporal = new_univariate_params(names(temporal_neighbours), fill = 0),
        temporal_spatial = pl$params$temporal_spatial,
        temporal_link = new_bivariate_params(names(temporal_neighbours), names(link_neighbours), fill = 0)
      ), 
      sample_df_ts,
      list(
        temporal = regulariser_strengths[a],
        spatial = regulariser_strengths[a],
        link = 5*regulariser_strengths[a]
      ), 
      "temporal_spatial",
      save_dir = save_dir, save_name = paste0(regulariser_strengths[a], ".rds"), force_new_session_params = T, 
      clean_lr = 0.1, sample_size = 10000, save_every = 5, num_values = 20)
    
  } else {
    # new params function changes!
    train(
      x_ts, obj_fx_builder, mse_ts, 
      params = list(
        temporal = new_univariate_params(names(temporal_neighbours), fill = 0),
        temporal_spatial = new_bivariate_params(names(temporal_neighbours), names(spatial_neighbours), fill = 0),
        temporal_link = new_bivariate_params(names(temporal_neighbours), names(link_neighbours), fill = 0)
      ), 
      sample_df_ts,
      list(
        temporal = regulariser_strengths[a],
        spatial = regulariser_strengths[a],
        link = 5*regulariser_strengths[a]
      ), 
      "temporal_spatial",
      save_dir = save_dir, save_name = paste0(regulariser_strengths[a], ".rds"), force_new_session_params = T, 
      clean_lr = 0.1, sample_size = 10000, save_every = 5, num_values = 20)
  }
}

# Now train temporal-link parameters after temporal-spatial parameters have converged
pl_ts_converged <- readRDS(file.path(save_dir, paste0("1.rds")))
for (a in 1 : length(regulariser_strengths))  {
  
  if (a != 1) {
    
    # Use previous parameters as starting point
    pl <- readRDS(file.path(save_dir, paste0(regulariser_strengths[a-1], ".rds")))
    
    train(
      x_tl, obj_fx_builder, mse_tl, 
      params = list(
        temporal = new_univariate_params(names(temporal_neighbours), fill = 0),
        temporal_spatial = pl_ts_converged$params$temporal_spatial,
        temporal_link = pl$params$temporal_link
      ), 
      sample_df_tl,
      list(
        temporal = regulariser_strengths[a],
        spatial = regulariser_strengths[a],
        link = 5*regulariser_strengths[a]
      ), 
      "temporal_link",
      save_dir = save_dir, save_name = paste0(regulariser_strengths[a], ".rds"), force_new_session_params = T, 
      clean_lr = 0.1, sample_size = 10000, save_every = 5, num_values = 20)
    
  } else {
    # new params function changes!
    train(
      x_tl, obj_fx_builder, mse_tl, 
      params = list(
        temporal = new_univariate_params(names(temporal_neighbours), fill = 0),
        temporal_spatial = new_bivariate_params(names(temporal_neighbours), names(spatial_neighbours), fill = 0),
        temporal_link = new_bivariate_params(names(temporal_neighbours), names(link_neighbours), fill = 0)
      ), 
      sample_df_tl,
      list(
        temporal = regulariser_strengths[a],
        spatial = regulariser_strengths[a],
        link = 5*regulariser_strengths[a]
      ), 
      "temporal_link",
      save_dir = save_dir, save_name = paste0(regulariser_strengths[a], ".rds"), force_new_session_params = T, 
      clean_lr = 0.1, sample_size = 10000, save_every = 5, num_values = 20)
  }
}