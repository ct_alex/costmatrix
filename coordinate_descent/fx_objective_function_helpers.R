get_univariate_regulariser_function <- function (x_fixed, var_params, var_neighbours, var_z, var_strength, laplace = F) {
  
  neighbour_param_values <- var_params[var_neighbours[[x_fixed]]]
  neighbour_weights <- map_dbl(var_neighbours[[x_fixed]], var_z)
  
  if (laplace) {
    
    return (function (param_values) {
      return (map_dbl(param_values, function (pv) {
        var_strength * sum(abs((pv - neighbour_param_values) * neighbour_weights))
      }))
    })
    
  } else {
    
    return (function (param_values) {
      return (map_dbl(param_values, function (pv) {
        var_strength * abs(sum((pv - neighbour_param_values) * neighbour_weights))
      }))
    })
  }
}

get_bivariate_regulariser_function <- function (x_row, x_col, var_params, row_neighbours, col_neighbours, var_z, row_strength, col_strength, laplace = F) {
  
  row_neighbour_param_values <- var_params[row_neighbours[[x_row]], x_col]
  row_neighbour_weights <- map2_dbl(row_neighbours[[x_row]], rep(x_col, length(row_neighbours[[x_row]])), var_z)
  
  col_neighbour_param_values <- var_params[x_row, col_neighbours[[x_col]]]
  col_neighbour_weights <- map2_dbl(rep(x_row, length(col_neighbours[[x_col]])), col_neighbours[[x_col]], var_z)
  
  if (laplace) {
    
    return (function (param_values) {
      return (
        map_dbl(param_values, function (pv) {
          row_strength * sum(abs((pv - row_neighbour_param_values) * row_neighbour_weights)) + 
            col_strength * sum(abs((pv - col_neighbour_param_values) * col_neighbour_weights))
        })
      )
    })
    
  } else {
    
    return (function (param_values) {
      return (map_dbl(param_values, function (pv) {
        row_strength * abs(sum((pv - row_neighbour_param_values) * row_neighbour_weights)) + 
          col_strength * abs(sum((pv - col_neighbour_param_values) * col_neighbour_weights))
      }))
    })
  }
}
