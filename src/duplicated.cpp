#include <iostream>
#include <queue>
#include <vector>
#include <fstream>
#include <limits>
#include <functional>
#include <algorithm>
#include <Rcpp.h>

using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]

template <typename T> inline double simple_objective(T x) {
  
  return x[0];
}

// [[Rcpp::export]]
Rcpp::DataFrame Remove_duplicate(std::vector<int> gfrom, std::vector<int> gto, std::vector<double> gw, int NbNodes){
  
  std::vector<std::vector<std::pair<int, double> > > Graph(NbNodes);
  
  bool add_to_graph;
  for (unsigned int i=0; i < gfrom.size(); ++i) {
    
    add_to_graph = true;
    if (Graph[gfrom[i]].size() == 0) {
      
      Graph[gfrom[i]].push_back(std::make_pair(gto[i], gw[i]));
    }
    else {
      
      for (unsigned int j=0; j < Graph[gfrom[i]].size(); ++j) {
        
        // If duplicate targets exist, choose the smallest weight of the two
        if (Graph[gfrom[i]][j].first == gto[i]) {
          add_to_graph = false;
          if (Graph[gfrom[i]][j].second >= gw[i]) {
            Graph[gfrom[i]][j].second = gw[i];
          }
          else break;
        }
      }
      
      if (add_to_graph) {
        Graph[gfrom[i]].push_back(std::make_pair(gto[i], gw[i]));
      }
    }
  }
  
  std::vector<int> from;
  std::vector<int> to;
  std::vector<double> w;
  
  for (unsigned int i=0; i < Graph.size(); ++i){
    for (unsigned int j=0; j < Graph[i].size(); ++j){
      from.push_back(i);
      to.push_back(Graph[i][j].first);
      w.push_back(Graph[i][j].second);
    }
  }
  
  Rcpp::DataFrame final = Rcpp::DataFrame::create(
    Rcpp::Named("from") = from,
    Rcpp::Named("to") = to,
    Rcpp::Named("dist") = w
  );
  
  return final;
}

// [[Rcpp::export]]
Rcpp::List Remove_duplicate_VOV(std::vector<int> gfrom, std::vector<int> gto, Rcpp::NumericMatrix &gw, int NbNodes) {
  
  std::vector<std::vector<std::pair<int, std::vector<double> > > > Graph(NbNodes);
  
  bool add_to_graph;
  int count = 0;
  for (unsigned int i=0; i < gfrom.size(); ++i) {
    
    add_to_graph = true;
    if (Graph[gfrom[i]].size() == 0) {
      
      // Rcpp::NumericVector gwi = gw(i, _);
      Rcpp::NumericVector nv = gw(i, _);
      // std::vector<double> sv = ;
      Graph[gfrom[i]].push_back(std::make_pair(gto[i], Rcpp::as<std::vector<double> > (nv)));
      count += 1;
    }
    else {
      
      for (unsigned int j=0; j < Graph[gfrom[i]].size(); ++j) {
        
        // If duplicate targets exist, choose the smallest weight of the two
        if (Graph[gfrom[i]][j].first == gto[i]) {
          
          add_to_graph = false;
          // if (Graph[gfrom[i]][j].second[0] >= gw(i, _)[0]) { // TODO: substitute [0] with function of two
          if (simple_objective(Graph[gfrom[i]][j].second) > simple_objective(gw(i, _))) {
            
            // Rcpp::NumericVector gwi = gw(i, _);
            Rcpp::NumericVector nv = gw(i, _);
            // std::vector<double> sv = Rcpp::as<std::vector<double> > (nv);
            Graph[gfrom[i]][j].second = Rcpp::as<std::vector<double> > (nv);
          }
          else break;
        }
      }
      
      if (add_to_graph) {
        
        // Rcpp::NumericVector gwi = gw(i, _);
        Rcpp::NumericVector nv = gw(i, _);
        // std::vector<double> sv = Rcpp::as<std::vector<double> > (nv);
        Graph[gfrom[i]].push_back(std::make_pair(gto[i], Rcpp::as<std::vector<double> > (nv)));
        count += 1;
      }
    }
  }
  
  std::cout << "Done with first part" << std::endl;
  
  std::vector<int> from;
  std::vector<int> to;
  // std::vector<double> w;
  Rcpp::NumericMatrix w(count, gw.ncol());
  count = 0;
  for (unsigned int i=0; i < Graph.size(); ++i) {
    
    for (unsigned int j=0; j < Graph[i].size(); ++j) {
      
      from.push_back(i);
      to.push_back(Graph[i][j].first);
      
      // w.push_back(Graph[i][j].second);
      // Rcpp::NumericMatrix::Row v = Graph[i][j].second;
      
      for (int k=0; k<Graph[i][j].second.size(); k++) {
        double x = Graph[i][j].second[k];
        w(count, k) = x;
      }
     
      count += 1;
    }
  }
  
  std::cout << "Done with second part" << std::endl;
  
  Rcpp::DataFrame final = Rcpp::DataFrame::create(
    Rcpp::Named("from") = from,
    Rcpp::Named("to") = to
    // Rcpp::Named("dist") = w
  );
  
  std::cout << "Done with third part" << std::endl;
  
  // Rcpp::NumericMatrix ww = w;
  
  Rcpp::List results(2);
  results[0] = final;
  results[1] = w;
  
  // delete w;
  
  std::cout << "Done with fourth part" << std::endl;
  
  return results;
}

