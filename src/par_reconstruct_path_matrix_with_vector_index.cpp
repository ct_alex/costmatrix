// [[Rcpp::depends(RcppParallel)]]
// [[Rcpp::plugins(cpp11)]]

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <stack>
#include <Rcpp.h>
#include <RcppParallel.h>

using namespace std;
using namespace RcppParallel;

struct MatParRePathVecInd : public Worker {
  
  const RcppParallel::RVector<int> m_shortcut_ids;
  const RcppParallel::RVector<int> m_from_indices;
  const RcppParallel::RVector<int> m_from_shortcuts;
  const RcppParallel::RVector<int> m_to_indices;
  const RcppParallel::RVector<int> m_to_shortcuts;
  const RcppParallel::RVector<int> m_ft_shortcut;
  
  // Output
  RcppParallel::RMatrix<double> m_holding_matrix;
  
  // Constructor
  MatParRePathVecInd(
    const Rcpp::IntegerVector &shortcut_ids,
    const Rcpp::IntegerVector &from_indices,
    const Rcpp::IntegerVector &from_shortcuts,
    const Rcpp::IntegerVector &to_indices,
    const Rcpp::IntegerVector &to_shortcuts,
    const Rcpp::IntegerVector &ft_shortcut,
    Rcpp::NumericMatrix holding_matrix)
    : m_shortcut_ids(shortcut_ids),
      m_from_indices(from_indices), m_from_shortcuts(from_shortcuts),
      m_to_indices(to_indices), m_to_shortcuts(to_shortcuts),
      m_ft_shortcut(ft_shortcut),
      m_holding_matrix(holding_matrix) {}
  
  //overload () operator
  void operator()(std::size_t begin, std::size_t end) {
    
    int h_idx;
    
    int shortcut_node;
    int compare_idx;
    int current_idx;
    int common_idx;
    
    int length_from;
    int length_to;
    
    bool current_has_shortcut;
    bool compare_has_shortcut;
    
    for (std::size_t k=begin; k!=end; k++) {
      
      std::stack<int> Q;
      Q.push(m_shortcut_ids[k+1]);
      
      current_idx = m_shortcut_ids[k];
      
      m_holding_matrix(k, 0) = current_idx;
      h_idx = 1;
      
      do {
        
        compare_idx = Q.top();
        Q.pop();
        
        length_from = m_from_indices[current_idx+1] - m_from_indices[current_idx];
        length_to = m_to_indices[compare_idx+1] - m_to_indices[compare_idx];
        
        current_has_shortcut = length_from > 0;
        compare_has_shortcut = length_to > 0;
        
        if (current_has_shortcut & compare_has_shortcut) {
          
          std::vector<int> result;
          
          if (length_from < length_to) {
            
            std::unordered_set<int> u_set(
                m_from_shortcuts.begin() + m_from_indices[current_idx], 
                                                         m_from_shortcuts.begin() + m_from_indices[current_idx] + length_from);
            
            for (int i=0; i < length_to; i++) {
              if (u_set.count(*(m_to_shortcuts.begin() + m_to_indices[compare_idx] + i))) {
                
                result.push_back(*(m_to_shortcuts.begin() + m_to_indices[compare_idx] + i));
                u_set.erase(*(m_to_shortcuts.begin() + m_to_indices[compare_idx] + i));
              }
            }
          }
          else {
            
            std::unordered_set<int> u_set(
                m_to_shortcuts.begin() + m_to_indices[compare_idx], 
                                                     m_to_shortcuts.begin() + m_to_indices[compare_idx] + length_to);
            for (int i=0; i<length_from; i++) {
              if (u_set.count(*(m_from_shortcuts.begin() + m_from_indices[current_idx] + i))) {
                
                result.push_back(*(m_from_shortcuts.begin() + m_from_indices[current_idx] + i));
                u_set.erase(*(m_from_shortcuts.begin() + m_from_indices[current_idx] + i));
              }
            }
          }
          
          if (result.size() == 1) common_idx = result[0];
          else common_idx = -1;
          
          if (common_idx != -1) {
            
            shortcut_node = m_ft_shortcut[common_idx];
            
            Q.push(compare_idx);
            Q.push(shortcut_node);
          }
          else {
            m_holding_matrix(k, h_idx) = compare_idx;
            current_idx = compare_idx;
            
            h_idx += 1;
          }
        }
        else {
          m_holding_matrix(k, h_idx) = compare_idx;
          current_idx = compare_idx;
          
          h_idx += 1;
        }
      } while (!Q.empty());
    }
  }
};

// [[Rcpp::export]]
Rcpp::List par_reconstruct_path_matrix_with_shortcut_indices(
  Rcpp::List &shortcut_id_list,
  const Rcpp::IntegerVector &from_indices,
  const Rcpp::IntegerVector &from_shortcuts,
  const Rcpp::IntegerVector &to_indices,
  const Rcpp::IntegerVector &to_shortcuts,
  const Rcpp::IntegerVector &ft_shortcut,
  int holding_length = 1500,  // maximum reconstructed path length between any two shortcut pairs
  int concat_size = 2000,  // maximum number of pair elements to parallelise 
  int concat_size_multiplier = 110,  // expected complete path length = concat_size*concat_size_multiplier
  int num_pairs = 1000  // maximum number of pairs required to fill concat vector
) {
  
  int f_idx;
  int h_idx;
  
  std::vector<int> row_indices(num_pairs);
  std::vector<int> col_indices(num_pairs);
  
  int ij_count = 0;
  std::vector<int> ij_idx(num_pairs);
  
  int concat_count = 0;
  std::vector<int> concat(concat_size);
  
  // Output
  Rcpp::List final_list(shortcut_id_list.size());
  
  for (int r = 0; r < shortcut_id_list.size(); r++) {
    
    SEXP pL_i(shortcut_id_list[r]);
    Rcpp::List L_i(pL_i);
    
    Rcpp::List final_list_r(L_i.size());
    
    for (int c = 0; c < L_i.size(); c++) {
      
      SEXP pL_ij(L_i[c]);
      
      /* i c ij_idx            concat             ij_count (scl) concat_count (scl)
       * 0 0 0      (0  -> 14) [234, 212, ..., 3] 0              0
       * 0 1 15     (15 -> 31) [34, 12, ..., 15]  1              15
       * 0 2 32     (None)     []                 2              32
       * 0 3 32     (32 -> 39) [90, 23, ..., 1]   3              32
       * 0 4 40     ...        ...                ...            ...
       * ... ...
       * 
       * None for empty or singular
       */
      
      if (Rf_isNull(pL_ij)) {
        
        row_indices[ij_count] = r;
        col_indices[ij_count] = c;
        ij_idx[ij_count] = concat_count;
        
        ij_count += 1;
        
        final_list_r[c] = -1;
        continue;
      }
      
      Rcpp::IntegerVector shortcut_ids(pL_ij);
      
      if (shortcut_ids.size() == 1) {
        
        row_indices[ij_count] = r;
        col_indices[ij_count] = c;
        ij_idx[ij_count] = concat_count;
        
        ij_count += 1;
        
        final_list_r[c] = shortcut_ids;
        continue;
      }
      
      // If length added to concat is less than concat_size, then add to concat and update all vectors
      
      // Else run reconstruct shortcut path
      
      if (concat_count + shortcut_ids.size() < concat_size) {
        
        row_indices[ij_count] = r;
        col_indices[ij_count] = c;
        ij_idx[ij_count] = concat_count;
        
        ij_count += 1;
        
        for (int s_i=0; s_i < shortcut_ids.size(); s_i++) {
          
          concat[concat_count] = shortcut_ids[s_i];
          concat_count += 1;
        }
      } 
      else {
        
        // Run reconstruct shortcut path
        
        Rcpp::NumericMatrix holding_matrix(shortcut_ids.size()-1, holding_length);
        holding_matrix = holding_matrix - 1;
        
        MatParRePathVecInd shortcutor(
            shortcut_ids,
            from_indices,
            from_shortcuts,
            to_indices,
            to_shortcuts,
            ft_shortcut,
            holding_matrix
        );
        parallelFor(0, shortcut_ids.size()-1, shortcutor);
        
        // Output
        std::vector<int> holding_final(concat_size*concat_size_multiplier, -1);
        
        f_idx = 0;
        for (unsigned int i=0; i < shortcut_ids.size()-1; i++) {
          
          h_idx = 0;
          while (holding_matrix(i, h_idx) != -1) {
            
            holding_final[f_idx] = holding_matrix(i, h_idx);
            h_idx += 1;
            f_idx += 1;
          }
          
          f_idx -= 1;
        }
        
        holding_final[f_idx] = holding_matrix(shortcut_ids.size()-2, h_idx-1);
        f_idx += 1;
        
        std::vector<int> result(f_idx);
        for (int i=0; i < f_idx; i++) {
          result[i] = holding_final[i];
        }
        
        final_list_r[c] = result;
      }
      
    }
    
    final_list[r] = final_list_r;
  }
  
  return final_list;
}





































