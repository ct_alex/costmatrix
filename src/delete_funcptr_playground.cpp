#include <Rcpp.h>
#include <vector>
using namespace Rcpp;

template <typename T>
  NumericVector core_processing(T func, double l) {
    double accum = 0;
    for (int i=0; i<1e3; i++)
      accum += sum(as<NumericVector>(func(3, l)));
      return NumericVector(1, accum);
  }

// double add(double x1, double x2) {
//   return x1 + x2;
// }

typedef SEXP (*funcPtr)(double, double);
// [[Rcpp::export]]
NumericVector execute_my_cpp(Rcpp::List function_pointers, NumericMatrix x) {
  
  std::vector<funcPtr> functions(function_pointers.size());
  for (unsigned int i=0; i < function_pointers.size(); i++) {
    
    SEXP pL_i(function_pointers[i]);
    funcPtr func = *XPtr<funcPtr>(pL_i);
    functions[i] = func;
  }
  
  Rcpp::NumericVector result(x.nrow(), 0.0);
  for (unsigned int i=0; i < x.nrow(); i++) {
    
    double result_i = 0.0;
    for (unsigned int j=0; j < functions.size(); j++) {
      
      result_i = result_i + as<double>(functions[j](x(i, 0), x(i, 1)));
    }
    
    result[i] = result_i;
  }
  
  return result;
}


