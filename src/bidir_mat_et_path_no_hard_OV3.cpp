#include <iostream>
#include <queue>
#include <vector>
#include <fstream>
#include <limits>
#include <functional>
#include <chrono>
#include <thread>
#include <Rcpp.h>
#include "atomic_structs.h"

using namespace std;
using namespace Rcpp;

// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::export]]
Rcpp::List Bidir_mat_et_path_no_hard_OV3(
    std::vector<int> &dep, 
    std::vector<int> &arr,
    std::vector<int> &gfrom,
    std::vector<int> &gto,
    Rcpp::NumericMatrix &gw,
    int NbNodes,
    std::vector<int> &Rank,
    bool route_minutes = true,
    bool route_meters = false,
    double intersection_penalty = 0.0,
    double prefer_higher_road_classes = 0.0,
    double prefer_min_backroads = 0.0,
    double prefer_min_tolls = 0.0,
    double prefer_min_rough_roads = 0.0,
    double prefer_min_bad_roads = 0.0,
    double prefer_min_bridges = 0.0,
    double prefer_min_tunnels = 0.0
) {
  
  objective_no_hard_functor obj_func(
      route_minutes,
      route_meters,
      intersection_penalty,
      prefer_higher_road_classes,
      prefer_min_backroads,
      prefer_min_tolls,
      prefer_min_rough_roads,
      prefer_min_bad_roads,
      prefer_min_bridges,
      prefer_min_tunnels
  );
  
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b) {
      return a.second > b.second;
    }
  };
  
  Rcpp::NumericMatrix result(arr.size(), dep.size());
  
  // Graphs
  int NbEdges = gfrom.size();
  
  std::vector<std::vector<std::pair<int, std::vector<double> > > > G(NbNodes);   
  std::vector<std::vector<std::pair<int, std::vector<double> > > > Gr(NbNodes);
  
  std::vector<std::vector<std::pair<int, std::vector<double> > > > G_full(NbNodes);
  std::vector<std::vector<std::pair<int, std::vector<double> > > > Gr_full(NbNodes);
  
  for (int i = 0; i < NbEdges; ++i) {
    
    Rcpp::NumericVector nv = gw(i, _);
    
    // Ranks are unique
    if (Rank[gfrom[i]] < Rank[gto[i]]) G[gfrom[i]].push_back(std::make_pair(
        gto[i], Rcpp::as<std::vector<double> >(nv)));
    if (Rank[gfrom[i]] > Rank[gto[i]]) Gr[gto[i]].push_back(std::make_pair(
        gfrom[i], Rcpp::as<std::vector<double> >(nv)));
  }
  
  // Forward
  std::vector<std::vector<std::pair<int, double> > > BucketF(NbNodes);
  
  std::vector<double> Distances(NbNodes, std::numeric_limits<double>::max()); 
  std::vector <int> Visited(NbNodes, 0);
  
  std::vector <std::vector <int> > Predecessors(dep.size(), std::vector <int> (NbNodes, 0));  // Try storing predecessors as vectors for now
  
  for (unsigned int k=0; k != dep.size(); k++) {
    
    if (k % 256) {
      Rcpp::checkUserInterrupt();
    }
    
    int StartNode = dep[k];
    Distances[StartNode] = 0.0;
    
    std::priority_queue<std::pair<int, double>, std::vector<std::pair<int, double> >, comp> Q;
    Q.push(std::make_pair(StartNode, 0.0));
    
    while (true) {  
      
      if (Q.empty()) {
        break;
      }
      else {
        
        int v = Q.top().first;
        double w = Q.top().second;
        Q.pop();
        
        Visited[v] = 1;
        
        if (w <= Distances[v]) {
          
          for (unsigned int i=0; i < G[v].size(); i++) {
            
            int v2 = G[v][i].first;                                                     
            double w2 = obj_func(G[v][i].second);
            
            if (Distances[v] + w2 < Distances[v2]) {  
              
              Distances[v2] = Distances[v] + w2;                                   
              Q.push(std::make_pair(v2, Distances[v2]));
              Visited[v2] = 1;
              
              Predecessors[k][v2] = v;
            }
          }
        }
      }
    }
    
    for (unsigned int i=0; i < Distances.size(); i++) {
      
      if (Distances[i] < std::numeric_limits<double>::max()) {
        BucketF[i].push_back(std::make_pair(k, Distances[i]));
      }
    }
    
    std::fill(Distances.begin(), Distances.end(), std::numeric_limits<double>::max());
  }
  
  for (unsigned int i=0; i < BucketF.size(); i++) {
    std::sort(BucketF[i].begin(), BucketF[i].end());
  }
  
  // Backward
  std::vector <std::vector <int> > Successors(arr.size(), std::vector <int> (NbNodes, 0));  // Try storing predecessors as vectors for now
  std::vector <std::vector <int> > Linkers(dep.size(), std::vector <int> (arr.size(), 0));  // Try storing predecessors as vectors for now
  for (unsigned int k=0; k != arr.size(); k++) {
    
    if (k % 256) {
      Rcpp::checkUserInterrupt();
    }
    
    Rcpp::NumericVector Dist(dep.size(), std::numeric_limits<double>::max());
    
    int StartNode = arr[k];
    Distances[StartNode] = 0.0;  
    
    std::priority_queue<std::pair<int, double>, std::vector<std::pair<int, double> >, comp> Q;
    Q.push(std::make_pair(StartNode, 0.0)); 
    
    while (true) {  
      
      if (Q.empty()) {
        break;
      }  
      
      if (!Q.empty()) {
        
        int v = Q.top().first;
        double w = Q.top().second;
        Q.pop();
        
        // Scan bucket
        if (Visited[v] > 0) {
          
          for (unsigned int i=0; i < BucketF[v].size(); i++) {
            
            int Source = BucketF[v][i].first;
            double D = Distances[v] + BucketF[v][i].second;
            
            if (D < Dist[Source]) {
              
              Dist[Source] = D;
              
              Linkers[Source][k] = v;
            }
          }
        }
        
        if (w <= Distances[v]) {
          
          for (unsigned int i=0; i < Gr[v].size(); i++) {
            
            int v2 = Gr[v][i].first;                                                     
            double w2 = obj_func(Gr[v][i].second);
            
            if (Distances[v] + w2 < Distances[v2]) {    
              
              Distances[v2] = Distances[v] + w2;                                   
              Q.push(std::make_pair(v2, Distances[v2]));
              
              Successors[k][v2] = v;
            }
          }
        }
      }
    }
    
    std::fill(Distances.begin(), Distances.end(), std::numeric_limits<double>::max());
    
    for (unsigned int i=0; i < Dist.size(); i++) {
      if (Dist[i] == std::numeric_limits<double>::max()) Dist[i] = Rcpp::NumericVector::get_na();
    }
    result.row(k) = Dist;
  }
  
  // Get (shortcut) paths
  Rcpp::List shortcut_paths(dep.size());
  for (unsigned int d=0; d < dep.size(); d++) {
    
    Rcpp::List shortcut_paths_d(arr.size());
    for (unsigned int a=0; a<arr.size(); a++) {
      
      if (Linkers[d][a] == 0) continue;
      
      // Successors : arr.size() x NbNodes
      // Predecessors : dep.size() x NbNodes
      
      // cout << "Finding path for " << d << " " << a << endl;
      // cout << "Finding path for " << dep[d] << " " << arr[a] << " " << Linkers[d][a] << endl;
      
      std::vector<int> shortcut_path;
      
      int next_v = Linkers[d][a];
      while (next_v != 0) {
        
        shortcut_path.push_back(next_v);
        next_v = Predecessors[d][next_v];
      }
      
      std::reverse(shortcut_path.begin(), shortcut_path.end());
      
      // std::vector<int> backward_path;
      next_v = Linkers[d][a];
      while (next_v != 0) {
        
        if (next_v != Linkers[d][a]) shortcut_path.push_back(next_v);
        next_v = Successors[a][next_v];
      }
      
      shortcut_paths_d[a] = shortcut_path;
    }
    
    shortcut_paths[d] = shortcut_paths_d;
  }
  
  Rcpp::List return_list(2);
  
  return_list[0] = Rcpp::transpose(result);
  return_list[1] = shortcut_paths;
  
  return return_list;
}
