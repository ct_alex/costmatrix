// [[Rcpp::depends(RcppParallel)]]
// [[Rcpp::plugins(cpp11)]]

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <stack>
#include <Rcpp.h>
#include <RcppParallel.h>

using namespace std;
using namespace RcppParallel;

struct ParRePathVecIndChunk : public Worker {
  
  const RcppParallel::RVector<int> m_from;
  const RcppParallel::RVector<int> m_to;
  const RcppParallel::RVector<int> m_from_indices;
  const RcppParallel::RVector<int> m_from_shortcuts;
  const RcppParallel::RVector<int> m_to_indices;
  const RcppParallel::RVector<int> m_to_shortcuts;
  const RcppParallel::RVector<int> m_ft_shortcut;
  
  // Output
  RcppParallel::RMatrix<double> m_holding_matrix;
  
  // Constructor
  ParRePathVecIndChunk(
    const Rcpp::IntegerVector &from,
    const Rcpp::IntegerVector &to,
    const Rcpp::IntegerVector &from_indices,
    const Rcpp::IntegerVector &from_shortcuts,
    const Rcpp::IntegerVector &to_indices,
    const Rcpp::IntegerVector &to_shortcuts,
    const Rcpp::IntegerVector &ft_shortcut,
    Rcpp::NumericMatrix holding_matrix)
    : m_from(from), m_to(to),
      m_from_indices(from_indices), m_from_shortcuts(from_shortcuts),
      m_to_indices(to_indices), m_to_shortcuts(to_shortcuts),
      m_ft_shortcut(ft_shortcut),
      m_holding_matrix(holding_matrix) {}
  
  //overload () operator
  void operator()(std::size_t begin, std::size_t end) {
    
    int h_idx;
    
    int shortcut_node;
    int compare_idx;
    int current_idx;
    int common_idx;
    
    int length_from;
    int length_to;
    
    bool current_has_shortcut;
    bool compare_has_shortcut;
    
    for (std::size_t k=begin; k!=end; k++) {
      
      std::stack<int> Q;
      Q.push(m_to[k]);
      
      current_idx = m_from[k];
      
      m_holding_matrix(k, 0) = current_idx;
      h_idx = 1;
      
      do {
        
        compare_idx = Q.top();
        Q.pop();
        
        length_from = m_from_indices[current_idx+1] - m_from_indices[current_idx];
        length_to = m_to_indices[compare_idx+1] - m_to_indices[compare_idx];
        
        current_has_shortcut = length_from > 0;
        compare_has_shortcut = length_to > 0;
        
        if (current_has_shortcut & compare_has_shortcut) {
          
          std::vector<int> result;
          
          if (length_from < length_to) {
            
            std::unordered_set<int> u_set(
                m_from_shortcuts.begin() + m_from_indices[current_idx], 
                                                         m_from_shortcuts.begin() + m_from_indices[current_idx] + length_from);
            
            for (int i=0; i < length_to; i++) {
              if (u_set.count(*(m_to_shortcuts.begin() + m_to_indices[compare_idx] + i))) {
                
                result.push_back(*(m_to_shortcuts.begin() + m_to_indices[compare_idx] + i));
                u_set.erase(*(m_to_shortcuts.begin() + m_to_indices[compare_idx] + i));
              }
            }
          }
          else {
            
            std::unordered_set<int> u_set(
                m_to_shortcuts.begin() + m_to_indices[compare_idx], 
                                                     m_to_shortcuts.begin() + m_to_indices[compare_idx] + length_to);
            for (int i=0; i<length_from; i++) {
              if (u_set.count(*(m_from_shortcuts.begin() + m_from_indices[current_idx] + i))) {
                
                result.push_back(*(m_from_shortcuts.begin() + m_from_indices[current_idx] + i));
                u_set.erase(*(m_from_shortcuts.begin() + m_from_indices[current_idx] + i));
              }
            }
          }
          
          if (result.size() == 1) common_idx = result[0];
          else common_idx = -1;
          
          if (common_idx != -1) {
            
            shortcut_node = m_ft_shortcut[common_idx];
            
            Q.push(compare_idx);
            Q.push(shortcut_node);
          }
          else {
            m_holding_matrix(k, h_idx) = compare_idx;
            current_idx = compare_idx;
            
            h_idx += 1;
          }
        }
        else {
          m_holding_matrix(k, h_idx) = compare_idx;
          current_idx = compare_idx;
          
          h_idx += 1;
        }
      } while (!Q.empty());
    }
  }
};

// [[Rcpp::export]]
Rcpp::List par_reconstruct_path_with_shortcut_indices_chunks(
    const Rcpp::IntegerVector &from,
    const Rcpp::IntegerVector &to,
    const Rcpp::IntegerVector &from_indices,
    const Rcpp::IntegerVector &from_shortcuts,
    const Rcpp::IntegerVector &to_indices,
    const Rcpp::IntegerVector &to_shortcuts,
    const Rcpp::IntegerVector &ft_shortcut,
    int holding_length = 2000,
    int final_holding_length = 10000
) {
  
  Rcpp::NumericMatrix holding_matrix(from.size(), holding_length);
  holding_matrix = holding_matrix - 1;
  
  ParRePathVecIndChunk shortcutor(
      from,
      to,
      from_indices,
      from_shortcuts,
      to_indices,
      to_shortcuts,
      ft_shortcut,
      holding_matrix
  );
  
  parallelFor(0, from.size(), shortcutor);
  
  // Output
  Rcpp::List shortcut_paths(from.size());
  Rcpp::List shortcut_lengths(from.size());
  
  // std::vector<int> holding_final(final_holding_length, -1);
  
  int h_idx;
  // int f_idx = 0;
  for (unsigned int i=0; i < from.size(); i ++) {
    
    h_idx = 0;
    while (holding_matrix(i, h_idx) != -1) {
      h_idx += 1;
    }
    
    shortcut_lengths[i] = h_idx;
    std::vector<int> final_h(h_idx);
    for (int h=0; h < h_idx; h++) {
      
      final_h[h] = holding_matrix(i, h);
    }
    
    shortcut_paths[i] = final_h;
  }
  
  Rcpp::List result(2);
  
  result[0] = shortcut_paths;
  result[1] = shortcut_lengths;
  
  return result;
}





































