// [[Rcpp::plugins(cpp11)]]

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <stack>
#include <Rcpp.h>

struct isIndex {
  
  int index;
  
  isIndex(int idx) : index(idx) {};
  
  bool operator()(const int i) {
    return i == index;
  }
};

inline std::vector<int> get_shortcut_indices (int index, std::vector<int> &shortcut_indices) {
  
  std::vector<int> result;
  
  std::vector<int>::iterator iter = shortcut_indices.begin();
  while ((iter= std::find_if(iter, shortcut_indices.end(), isIndex(index))) != shortcut_indices.end()) {
    
    result.push_back(std::distance(shortcut_indices.begin(), iter));
    iter++;
  }
  
  return result;
}

// [[Rcpp::export]]
Rcpp::List make_shortcuts (
  int n_nodes,
  std::vector<int> &shortcut_nodes,
  std::vector<int> &from,
  std::vector<int> &to,
  std::vector<int> &shortcut) {
  
  std::vector<int> from_indices(n_nodes + 1);
  std::vector<int> to_indices(n_nodes + 1);
  
  std::vector<int> from_shortcuts(from.size());
  std::vector<int> to_shortcuts(to.size());
  
  std::vector<int> idx;
  
  int idx_from = 0;
  int idx_to = 0;
  for (int i=0; i < n_nodes; i++) {
    
    from_indices[i] = idx_from;
    to_indices[i] = idx_to;
    
    idx = get_shortcut_indices(i, from);
    for (unsigned int i=0; i < idx.size(); i++) {
      
      from_shortcuts[idx_from] = idx[i];
      idx_from += 1;
    }
    
    idx = get_shortcut_indices(i, to);
    for (unsigned int i=0; i < idx.size(); i++) {
      
      to_shortcuts[idx_to] = idx[i];
      idx_to += 1;
    }
  }
  
  from_indices[n_nodes] = idx_from;
  to_indices[n_nodes] = idx_to;
  
  Rcpp::List result(5);
  result[0] = from_indices;
  result[1] = from_shortcuts;
  result[2] = to_indices;
  result[3] = to_shortcuts;
  result[4] = shortcut;
  
  return result;
}
