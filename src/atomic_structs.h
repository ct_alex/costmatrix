#ifndef ATOMIC_STRUCTS_H
#define ATOMIC_STRUCTS_H

struct atomic_min_avoid {
  
  int index;
  double min_value;
  
  atomic_min_avoid (int index_, double min_value_):
    index(index_), min_value(min_value_) {}
  
  bool operator() (std::vector<double> x) {
    return x[index] < min_value;
  }
};

struct atomic_max_avoid {
  
  int index;
  double max_value;
  
  atomic_max_avoid (int index_, double max_value_):
    index(index_), max_value(max_value_) {}
  
  bool operator() (std::vector<double> x) {
    return x[index] > max_value;
  }
};

struct atomic_exact_avoid {
  
  int index;
  double exact_value;
  
  atomic_exact_avoid (int index_, double exact_value_):
    index(index_), exact_value(exact_value_) {}
  
  bool operator() (std::vector<double> x) {
    return x[index] == exact_value;
  }
};

struct atomic_max_prefer {
  
  int index;
  double max_value;
  double penalty;
  
  atomic_max_prefer (int index_, double max_value_, double penalty_):
    index(index_), max_value(max_value_), penalty(penalty_) {}
  
  double operator() (std::vector<double> x) {
    if (x[index] > max_value) return penalty;
    return 0.0;
  }
};

struct atomic_min_prefer {
  
  int index;
  double min_value;
  double penalty;
  
  atomic_min_prefer (int index_, double min_value_, double penalty_):
    index(index_), min_value(min_value_), penalty(penalty_) {}
  
  double operator() (std::vector<double> x) {
    if (x[index] < min_value) return penalty;
    return 0.0;
  }
};

struct atomic_exact_prefer {
  
  int index;
  double exact_value;
  double penalty;
  
  atomic_exact_prefer (int index_, double exact_value_, double penalty_):
    index(index_), exact_value(exact_value_), penalty(penalty_) {}
  
  double operator() (std::vector<double> x) {
    if (x[index] == exact_value) return penalty;
    return 0.0;
  }
};

struct atomic_max_prefer_add {
  
  int index;
  double value;
  double penalty;
  
  atomic_max_prefer_add (int index_, double value_, double penalty_):
    index(index_), value(value_), penalty(penalty_) {}
  
  double operator() (std::vector<double> x) {
    if (x[index] > value) return penalty;
    return 0.0;
  }
};

struct atomic_add_per_unit {
  
  int index;
  double penalty;
  
  atomic_add_per_unit (int index_, double penalty_):
    index(index_), penalty(penalty_) {}
  
  double operator() (std::vector<double> x) {
    return x[index]*penalty;
  }
};

struct objective_literals {
  
  bool minutes;
  bool meters;
  
  objective_literals (bool minutes_, bool meters_):
    minutes(minutes_), meters(meters_) {}
  
  double operator() (std::vector<double> x) {
    if (minutes & meters) return x[0]/60 + x[1]/1000;
    if (minutes) return x[0];
    return x[1];
  }
};

struct objective_functor {
  
  // Literals (to complete...)
  bool route_minutes;
  bool route_meters;
  
  // Additive
  double intersection_penalty;
  
  // Preference
  // additive or multiplicative
  double prefer_higher_road_classes; // multiplicative min_prefer(4, 2.0)
  double prefer_min_backroads; // multiplicative max_prefer(6, 0.0)
  double prefer_min_tolls; // additive max_prefer_add(7, 0.0)
  double prefer_min_rough_roads; // multiplicative max_prefer(7, 0.0)
  double prefer_min_bad_roads; // multiplicative max_prefer(14, 1.0)
  double prefer_min_bridges; // multiplicative exact_prefer(11, 2.0)
  double prefer_min_tunnels; // multiplicative exact_prefer(11, 1.0)
  
  // Defaults
  bool avoid_forward_blocked_passage; // max_avoid(8, 0.0)
  bool avoid_backward_blocked_passage; // max_avoid(9, 0.0)
  
  // Avoids
  bool avoid_higher_road_classes; // min_avoid(4, 2.0)
  bool avoid_backroads; // max_avoid(6, 0.0)
  bool avoid_tolls; // max_avoid(7, 0.0)
  bool avoid_rough_roads; // exact_avoid(10, 1.0)
  bool avoid_bridges; // exact_avoid(11, 2.0)
  bool avoid_tunnels; // exact_avoid(11, 1.0)
  
  // For main objective
  objective_literals obj_lit;
  
  // For atomic functions
  std::vector<atomic_add_per_unit> add_per_unit_list;
  std::vector<atomic_min_avoid> min_avoid_list;
  std::vector<atomic_max_avoid> max_avoid_list;
  std::vector<atomic_exact_avoid> exact_avoid_list;
  std::vector<atomic_max_prefer> max_prefer_list;
  std::vector<atomic_min_prefer> min_prefer_list;
  std::vector<atomic_exact_prefer> exact_prefer_list;
  std::vector<atomic_max_prefer_add> max_prefer_add_list;
  
  objective_functor (
      bool route_minutes_ = true,
      bool route_meters_ = false,
      double intersection_penalty_ = 0.0,
      double prefer_higher_road_classes_ = 0.0,
      double prefer_min_backroads_ = 0.0,
      double prefer_min_tolls_ = 0.0,
      double prefer_min_rough_roads_ = 0.0,
      double prefer_min_bad_roads_ = 0.0,
      double prefer_min_bridges_ = 0.0,
      double prefer_min_tunnels_ = 0.0,
      bool avoid_forward_blocked_passage_ = false,
      bool avoid_backward_blocked_passage_ = false,
      bool avoid_higher_road_classes_ = false,
      bool avoid_backroads_ = false,
      bool avoid_tolls_ = false,
      bool avoid_rough_roads_ = false,
      bool avoid_bridges_ = false,
      bool avoid_tunnels_ = false
  ): obj_lit(route_minutes_, route_meters_) {
    
    // objective_literals obj_lit(route_minutes_, route_meters_);
    
    if (intersection_penalty_ != 0.) {
      atomic_add_per_unit aapu_intersection_penalty(15, intersection_penalty_);
      add_per_unit_list.push_back(aapu_intersection_penalty);
    }
    if (prefer_higher_road_classes_ != 0.0) {
      atomic_min_prefer amp_higher_road_classes(4, 2.0, prefer_higher_road_classes_);
      min_prefer_list.push_back(amp_higher_road_classes);
    }
    if (prefer_min_backroads_ != 0.0) {
      atomic_max_prefer amp_min_backroads(6, 0.0, prefer_min_backroads_);
      max_prefer_list.push_back(amp_min_backroads);
    }
    if (prefer_min_tolls_ != 0.0) {
      atomic_max_prefer_add ampa_min_tolls(7, 0.0, prefer_min_tolls_);
      max_prefer_add_list.push_back(ampa_min_tolls);
    }
    if (prefer_min_rough_roads_ != 0.0) {
      atomic_max_prefer amp_min_rough_roads(7, 0.0, prefer_min_rough_roads_);
      max_prefer_list.push_back(amp_min_rough_roads);
    }
    if (prefer_min_bad_roads_ != 0.0) {
      atomic_max_prefer amp_min_bad_roads(14, 1.0, prefer_min_bad_roads_);
      max_prefer_list.push_back(amp_min_bad_roads);
    }
    if (prefer_min_bridges_ != 0.0) {
      atomic_exact_prefer aep_min_bridges(11, 2.0, prefer_min_bridges_);
      exact_prefer_list.push_back(aep_min_bridges);
    }
    if (prefer_min_tunnels_ != 0.0) {
      atomic_exact_prefer aep_min_tunnels(11, 1.0, prefer_min_tunnels_);
      exact_prefer_list.push_back(aep_min_tunnels);
    }
    if (avoid_forward_blocked_passage_) {
      atomic_max_avoid ama_forward_blocked_passage(8, 0.0);
      max_avoid_list.push_back(ama_forward_blocked_passage);
    }
    if (avoid_backward_blocked_passage_) {
      atomic_max_avoid ama_backward_blocked_passage(9, 0.0);
      max_avoid_list.push_back(ama_backward_blocked_passage);
    }
    if (avoid_higher_road_classes_) {
      atomic_min_avoid ama_higher_road_classes(4, 2.0);
      min_avoid_list.push_back(ama_higher_road_classes);
    }
    if (avoid_backroads_) {
      atomic_max_avoid ama_backroads(6, 0.0);
      max_avoid_list.push_back(ama_backroads);
    }
    if (avoid_tolls_) {
      atomic_max_avoid ama_avoid_tolls(7, 0.0);
      max_avoid_list.push_back(ama_avoid_tolls);
    }
    if (avoid_rough_roads_) {
      atomic_exact_avoid aea_rough_roads(10, 1.0);
      exact_avoid_list.push_back(aea_rough_roads);
    }
    if (avoid_bridges_) {
      atomic_exact_avoid aea_bridges(11, 2.0);
      exact_avoid_list.push_back(aea_bridges);
    }
    if (avoid_tunnels_) {
      atomic_exact_avoid aea_tunnels(11, 1.0);
      exact_avoid_list.push_back(aea_tunnels);
    }
  };
  
  double operator() (std::vector<double> x) {
    
    for (int i=0; i < min_avoid_list.size(); i++) {
      if (min_avoid_list[i](x)) return std::numeric_limits<double>::max();
    }
    for (int i=0; i < max_avoid_list.size(); i++) {
      if (max_avoid_list[i](x)) return std::numeric_limits<double>::max();
    }
    for (int i=0; i < exact_avoid_list.size(); i++) {
      if (exact_avoid_list[i](x)) return std::numeric_limits<double>::max();
    }
    
    double multiplier = 1.0;
    for (int i=0; i < max_prefer_list.size(); i++) {
      multiplier += max_prefer_list[i](x);
    }
    for (int i=0; i < min_prefer_list.size(); i++) {
      multiplier += min_prefer_list[i](x);
    }
    for (int i=0; i < exact_prefer_list.size(); i++) {
      multiplier += exact_prefer_list[i](x);
    }
    
    double add_obj = 0.0;
    for (int i=0; i < max_prefer_add_list.size(); i++) {
      add_obj += max_prefer_add_list[i](x);
    }
    for (int i=0; i < add_per_unit_list.size(); i++) {
      add_obj += add_per_unit_list[i](x);
    }
    
    return obj_lit(x)*multiplier + add_obj;
  }
};

struct objective_no_hard_functor {
  
  // Literals (to complete...)
  bool route_minutes;
  bool route_meters;
  
  // Additive
  double intersection_penalty;
  
  // Preference
  // additive or multiplicative
  double prefer_higher_road_classes; // multiplicative min_prefer(4, 2.0)
  double prefer_min_backroads; // multiplicative max_prefer(6, 0.0)
  double prefer_min_tolls; // additive max_prefer_add(7, 0.0)
  double prefer_min_rough_roads; // multiplicative max_prefer(7, 0.0)
  double prefer_min_bad_roads; // multiplicative max_prefer(14, 1.0)
  double prefer_min_bridges; // multiplicative exact_prefer(11, 2.0)
  double prefer_min_tunnels; // multiplicative exact_prefer(11, 1.0)
  
  // For main objective
  objective_literals obj_lit;
  
  // For atomic functions
  std::vector<atomic_add_per_unit> add_per_unit_list;
  std::vector<atomic_max_prefer> max_prefer_list;
  std::vector<atomic_min_prefer> min_prefer_list;
  std::vector<atomic_exact_prefer> exact_prefer_list;
  std::vector<atomic_max_prefer_add> max_prefer_add_list;
  
  objective_no_hard_functor (
      bool route_minutes_,
      bool route_meters_,
      double intersection_penalty_,
      double prefer_higher_road_classes_,
      double prefer_min_backroads_,
      double prefer_min_tolls_,
      double prefer_min_rough_roads_,
      double prefer_min_bad_roads_,
      double prefer_min_bridges_,
      double prefer_min_tunnels_
  ): obj_lit(route_minutes_, route_meters_) {
    
    // objective_literals obj_lit(route_minutes_, route_meters_);
    
    if (intersection_penalty_ != 0.) {
      atomic_add_per_unit aapu_intersection_penalty(15, intersection_penalty_);
      add_per_unit_list.push_back(aapu_intersection_penalty);
    }
    if (prefer_higher_road_classes_ != 0.0) {
      atomic_min_prefer amp_higher_road_classes(4, 2.0, prefer_higher_road_classes_);
      min_prefer_list.push_back(amp_higher_road_classes);
    }
    if (prefer_min_backroads_ != 0.0) {
      atomic_max_prefer amp_min_backroads(6, 0.0, prefer_min_backroads_);
      max_prefer_list.push_back(amp_min_backroads);
    }
    if (prefer_min_tolls_ != 0.0) {
      atomic_max_prefer_add ampa_min_tolls(7, 0.0, prefer_min_tolls_);
      max_prefer_add_list.push_back(ampa_min_tolls);
    }
    if (prefer_min_rough_roads_ != 0.0) {
      atomic_max_prefer amp_min_rough_roads(7, 0.0, prefer_min_rough_roads_);
      max_prefer_list.push_back(amp_min_rough_roads);
    }
    if (prefer_min_bad_roads_ != 0.0) {
      atomic_max_prefer amp_min_bad_roads(14, 1.0, prefer_min_bad_roads_);
      max_prefer_list.push_back(amp_min_bad_roads);
    }
    if (prefer_min_bridges_ != 0.0) {
      atomic_exact_prefer aep_min_bridges(11, 2.0, prefer_min_bridges_);
      exact_prefer_list.push_back(aep_min_bridges);
    }
    if (prefer_min_tunnels_ != 0.0) {
      atomic_exact_prefer aep_min_tunnels(11, 1.0, prefer_min_tunnels_);
      exact_prefer_list.push_back(aep_min_tunnels);
    }
  };
  
  double operator() (std::vector<double> x) {
    
    double multiplier = 1.0;
    for (int i=0; i < max_prefer_list.size(); i++) {
      multiplier += max_prefer_list[i](x);
    }
    for (int i=0; i < min_prefer_list.size(); i++) {
      multiplier += min_prefer_list[i](x);
    }
    for (int i=0; i < exact_prefer_list.size(); i++) {
      multiplier += exact_prefer_list[i](x);
    }
    
    double add_obj = 0.0;
    for (int i=0; i < max_prefer_add_list.size(); i++) {
      add_obj += max_prefer_add_list[i](x);
    }
    for (int i=0; i < add_per_unit_list.size(); i++) {
      add_obj += add_per_unit_list[i](x);
    }
    
    return obj_lit(x)*multiplier + add_obj;
  }
};

#endif