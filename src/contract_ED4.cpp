#include <iostream>
#include <queue>
#include <vector>
#include <fstream>
#include <limits>
#include <functional>
#include <chrono>
#include <thread>
#include <progress.hpp>
#include <progress_bar.hpp>
#include <Rcpp.h>
#include "functions_one_to_all_ED4.h"

using namespace std;
using namespace Rcpp;

Rcpp::List extract_graph (
    int num_edges, 
    std::vector<std::vector<std::pair<int, double> > > &Graph, 
    std::vector<std::vector<std::pair<int, double> > > &Graphr
) {
  
  Rcpp::NumericMatrix w(num_edges, 3);
  int count = 0;
  for (unsigned int i=0; i < Graph.size(); ++i) {
    for (unsigned int j=0; j < Graph[i].size(); ++j) {
      
      w(count, 0) = i;
      w(count, 1) = Graph[i][j].first;
      w(count, 2) = Graph[i][j].second;
      
      count += 1;
    }
  }
  
  Rcpp::NumericMatrix wr(num_edges, 3);
  count = 0;
  for (unsigned int i=0; i < Graphr.size(); ++i) {
    for (unsigned int j=0; j < Graphr[i].size(); ++j) {
      
      wr(count, 0) = i;
      wr(count, 1) = Graphr[i][j].first;
      wr(count, 2) = Graphr[i][j].second;
      
      count += 1;
    }
  }
  
  Rcpp::List result(2);
  result[0] = w;
  result[1] = wr;
  
  return result;
}

Rcpp::List extract_graph_OV (
    int num_edges, 
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graph, 
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graphr
) {
  
  Rcpp::NumericMatrix w(num_edges, 3);
  int count = 0;
  for (unsigned int i=0; i < Graph.size(); ++i) {
    for (unsigned int j=0; j < Graph[i].size(); ++j) {
      
      w(count, 0) = i;
      w(count, 1) = Graph[i][j].first;
      w(count, 2) = Graph[i][j].second[0];
      
      count += 1;
    }
  }
  
  Rcpp::NumericMatrix wr(num_edges, 3);
  count = 0;
  for (unsigned int i=0; i < Graphr.size(); ++i) {
    for (unsigned int j=0; j < Graphr[i].size(); ++j) {
      
      wr(count, 0) = i;
      wr(count, 1) = Graphr[i][j].first;
      wr(count, 2) = Graphr[i][j].second[0];
      
      count += 1;
    }
  }
  
  Rcpp::List result(2);
  result[0] = w;
  result[1] = wr;
  
  return result;
}

// [[Rcpp::depends(RcppProgress)]]
// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::export]]
Rcpp::List Contract_ED_VOV(
    std::vector<int> &gfrom,
    std::vector<int> &gto,
    Rcpp::NumericMatrix &gw, // std::vector<double> &gw,
    int NbNodes,
    bool display_progress){
  
  int n_weights = gw.ncol();
  
  // Comparator for edge importance. Therefore, do not touch
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b) {
      return a.second > b.second;
    }
  };
  
  // Graph
  int NbEdges = gfrom.size();
  std::vector<std::vector<std::pair<int, std::vector<double> > > > G(NbNodes);
  for (int i = 0; i < NbEdges; ++i) {
    
    Rcpp::NumericVector nv = gw(i, _);
    // std::vector<double> sv = Rcpp::as<std::vector<double> >(nv);
    G[gfrom[i]].push_back(std::make_pair(gto[i], Rcpp::as<std::vector<double> >(nv)));
  }
  
  // Clean graph of erroneous shortcuts
  std::vector<double> Distances(NbNodes, std::numeric_limits<double>::max());   
  std::vector<std::vector<std::pair<int, std::vector<double> > > > OriginalGraph = G;
  
  if (display_progress) Rcpp::Rcout << "Cleaning graph..." << std::endl;
  
  for (int i=0; i < NbNodes; i++) {
    
    std::vector<int> arr;
    std::vector<std::vector<double> > lim;
    
    for (unsigned int j=0; j < G[i].size(); j++) {
      
      arr.push_back(G[i][j].first);
      lim.push_back(G[i][j].second);
    }
    
    if (arr.size()==0) continue;
    
    Dijkstra_mod2_VOV(
      G, OriginalGraph,
      i,
      arr, lim,
      NbNodes,
      Distances);
  }
  
  // Free space
  std::vector<std::vector<std::pair<int, std::vector<double> > > > ().swap(OriginalGraph);
  
  // Reversed graph
  // G := [from][to]
  // Gr := [to][from]
  std::vector<std::vector<std::pair<int, std::vector<double> > > > Gr(NbNodes);
  // int count = 0;
  for (unsigned int i=0; i < G.size(); ++i) {
    for (unsigned int j=0; j < G[i].size(); ++j) {
      
      Gr[G[i][j].first].push_back(std::make_pair(i, G[i][j].second));
      // count += 1;
    }
  }
  
  // return extract_graph_OV(count, G, Gr);
  
  // Debug
  // Rcpp::NumericVector debug(NbNodes);

  // Ordering
  if (display_progress) Rcpp::Rcout << "Initial node ordering..." << std::endl;

  priority_queue<std::pair<int, int>, vector<std::pair<int, int> >, comp> Queue;
  for (int i=0; i < NbNodes; i++) {

    Rcpp::checkUserInterrupt();

    // Edge_dif = shortcuts - (incoming + outgoing)
    // Edge_dif = 0 whenever incoming == 0 or outgoing == 0
    // max(shortcuts) = incoming*outgoing
    // TODO: Figure this out min(shortcuts)
    int ED = Edge_dif_VOV(i, G, Gr, NbNodes, Distances);

    Queue.push(std::make_pair(i, ED));

    // debug[i] = ED;
  }

  // return debug;

  // Contracting
  std::vector<int> Contracted(NbNodes, 0);

  // Final augmented graphs
  std::vector<std::vector<std::pair<int, std::vector<double> > > > AugG = G;
  std::vector<std::vector<std::pair<int, std::vector<double> > > > AugGr = Gr;

  // Shortcuts will be stored in 3 vectors (from, to, contracted node)
  std::vector<int> ShortF;
  std::vector<int> ShortT;
  std::vector<int> ShortC;

  // Free space
  std::vector<std::vector<std::pair<int, std::vector<double> > > >().swap(Gr);

  // Node order
  std::vector<int> Order(NbNodes, 0);

  int count = 0;
  bool error = false;

  if (display_progress) Rcpp::Rcout << "Contracting nodes..."<< std::endl;
  Progress p(NbNodes, display_progress);

  while(!Queue.empty()){

    if (error) break;

    Rcpp::checkUserInterrupt();

    // index with smallest ED score
    int v = Queue.top().first;
    Queue.pop();

    if (Contracted[v] == 1) {
      continue; // already contracted
    }

    int DN = 0;
    for (unsigned int i=0; i < G[v].size(); i++){
      if (Contracted[G[v][i].first] == 1) DN += 1;
    }

    // TODO: WHAT IS THIS?
    int new_imp = Edge_dif_VOV(v, AugG, AugGr, NbNodes, Distances)*190 + DN*120;

    if (Queue.empty()) {

      count += 1;
      p.increment();

      Order[v] = count;
      Contracted[v] = 1;

      Contract_VOV(
        v,
        AugG, AugGr, G,
        NbNodes,
        Distances, Contracted,
        count, error,
        ShortF, ShortT, ShortC);
    }
    else {

      if (new_imp > Queue.top().second) { //compare to the next min // TODO: WHY???

        Queue.push(std::make_pair(v, new_imp));
      }
      else {

        count += 1;
        p.increment();

        Order[v] = count;
        Contracted[v] = 1;

        Contract_VOV(
          v,
          AugG, AugGr, G,
          NbNodes,
          Distances, Contracted,
          count, error,
          ShortF, ShortT, ShortC);
      }
    }
  }

  std::cout << "Done with contraction" << std::endl;

  // Free space
  std::vector<std::vector<std::pair<int, std::vector<double> > > > ().swap(AugG);
  std::vector<std::vector<std::pair<int, std::vector<double> > > > ().swap(AugGr);

  // Augmented graph
  count = 0;
  for (int i=0; i < NbNodes; i++) count += G[i].size();

  std::vector<int> Newfrom(count);
  std::vector<int> Newto(count);
  Rcpp::NumericMatrix Neww(count, n_weights);

  int index = 0;
  for (int i=0; i < NbNodes; i++) {
    for (unsigned int j=0; j < G[i].size(); j++) {

      Rcpp::NumericVector nv = Rcpp::wrap(G[i][j].second);

      Newfrom[index] = i;
      Newto[index] = G[i][j].first;
      Neww(index, _) = nv;
      index += 1;
    }
  }

  std::cout << "Done with data prep" << std::endl;

  // Return
  Rcpp::List final(3);
  final[0] = Newfrom;
  final[1] = Newto;
  final[2] = Neww;

  Rcpp::List Shortcuts(3);
  Shortcuts[0] = ShortF;
  Shortcuts[1] = ShortT;
  Shortcuts[2] = ShortC;

  Rcpp::List finalList(3);
  finalList[0] = final;
  finalList[1] = Order;
  finalList[2] = Shortcuts;

  std::cout << "Done with final list" << std::endl;

  return (finalList);
}

// [[Rcpp::depends(RcppProgress)]]
// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::export]]
Rcpp::List Contract_ED(
    std::vector<int> &gfrom,
    std::vector<int> &gto,
    std::vector<double> &gw,
    int NbNodes,
    bool display_progress){
  
  // Comparator 
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b){
      return a.second > b.second;
    }
  };
  
  // Graph
  int NbEdges = gfrom.size();
  std::vector<std::vector<std::pair<int, double> > > G(NbNodes);
  for (int i = 0; i < NbEdges; ++i) {
    
    G[gfrom[i]].push_back(std::make_pair(gto[i], gw[i]));
  }
  
  // Clean graph of erroneous shortcuts
  std::vector<double> Distances(NbNodes, std::numeric_limits<double>::max());   
  std::vector<std::vector<std::pair<int, double> > > OriginalGraph = G;
  
  if (display_progress) Rcpp::Rcout << "Cleaning graph..." << std::endl;
  
  for (int i=0; i < NbNodes; i++) {
    
    std::vector<int> arr;
    std::vector<double> lim;
    
    for (unsigned int j=0; j < G[i].size();j++) {
      
      arr.push_back(G[i][j].first);
      lim.push_back(G[i][j].second);
    }
    
    if (arr.size()==0) continue;
    
    Dijkstra_mod2(G,
                  OriginalGraph,
                  i,
                  arr,
                  lim,
                  NbNodes,
                  Distances);
  }
  
  // Free space
  std::vector<std::vector<std::pair<int, double> > > ().swap(OriginalGraph);
  
  // Reversed graph
  // G := [from][to]
  // Gr := [to][from]
  std::vector<std::vector<std::pair<int, double> > > Gr(NbNodes);
  // int count = 0;
  for (unsigned int i=0; i < G.size(); ++i) {
    for (unsigned int j=0; j < G[i].size(); ++j) {
      
      Gr[G[i][j].first].push_back(std::make_pair(i, G[i][j].second));
      // count += 1;
    }
  }
  
  // return extract_graph(count, G, Gr);
  
  // Debug
  // Rcpp::NumericVector debug(NbNodes);

  // Ordering
  if (display_progress) Rcpp::Rcout << "Initial node ordering..." << std::endl;

  priority_queue<std::pair<int, int>, vector<std::pair<int, int> >, comp> Queue;
  for (int i=0; i < NbNodes; i++) {

    Rcpp::checkUserInterrupt();

    // Edge_dif = shortcuts - (incoming + outgoing)
    // Edge_dif = 0 whenever incoming == 0 or outgoing == 0
    // max(shortcuts) = incoming*outgoing
    // TODO: Figure this out min(shortcuts)
    int ED = Edge_dif(i, G, Gr, NbNodes, Distances);

    Queue.push(std::make_pair(i, ED));
    // debug[i] = ED;
  }

  // return debug;


  // Contracting
  std::vector<int> Contracted(NbNodes, 0);

  // Final augmented graphs
  std::vector<std::vector<std::pair<int, double> > > AugG = G;
  std::vector<std::vector<std::pair<int, double> > > AugGr = Gr;

  // Shortcuts will be stored in 3 vectors (from, to, contracted node)
  std::vector<int> ShortF;
  std::vector<int> ShortT;
  std::vector<int> ShortC;

  // Free space
  std::vector<std::vector<std::pair<int, double> > >().swap(Gr);

  // Node order
  std::vector<int> Order(NbNodes, 0);

  int count = 0;
  bool error = false;

  if (display_progress) Rcpp::Rcout << "Contracting nodes..."<< std::endl;
  Progress p(NbNodes, display_progress);

  while(!Queue.empty()){

    if (error) break;

    Rcpp::checkUserInterrupt();

    // index with smallest ED score
    int v = Queue.top().first;
    Queue.pop();

    if (Contracted[v] == 1) {
      continue; // already contracted
    }

    int DN = 0;
    for (unsigned int i=0; i < G[v].size(); i++){
      if (Contracted[G[v][i].first] == 1) DN += 1;
    }

    // TODO: WHAT IS THIS?
    int new_imp = Edge_dif(v, AugG, AugGr, NbNodes, Distances)*190 + DN*120;

    if (Queue.empty()) {

      count += 1;
      p.increment();

      Order[v] = count;
      Contracted[v] = 1;

      Contract(v,
               AugG, AugGr,
               G,
               NbNodes,
               Distances, Contracted,
               count, error,
               ShortF, ShortT, ShortC);
    }
    else{

      if (new_imp > Queue.top().second) { //compare to the next min // TODO: WHY???

        Queue.push(std::make_pair(v, new_imp));
      }
      else {

        count += 1;
        p.increment();

        Order[v] = count;
        Contracted[v] = 1;

        Contract(v,
                 AugG, AugGr,
                 G,
                 NbNodes,
                 Distances, Contracted,
                 count, error,
                 ShortF, ShortT, ShortC);
      }
    }
  }

  // Free space
  std::vector<std::vector<std::pair<int, double> > > ().swap(AugG);
  std::vector<std::vector<std::pair<int, double> > > ().swap(AugGr);

  // Augmented graph
  count = 0;
  for (int i=0; i < NbNodes; i++) count += G[i].size();

  std::vector<int> Newfrom(count);
  std::vector<int> Newto(count);
  std::vector<double> Neww(count);

  int index = 0;
  for (int i=0; i < NbNodes; i++) {
    for (unsigned int j=0; j < G[i].size(); j++) {

      Newfrom[index] = i;
      Newto[index] = G[i][j].first;
      Neww[index] = G[i][j].second;
      index += 1;
    }
  }

  // Return
  Rcpp::List final(3);
  final[0] = Newfrom;
  final[1] = Newto;
  final[2] = Neww;

  Rcpp::List Shortcuts(3);
  Shortcuts[0] = ShortF;
  Shortcuts[1] = ShortT;
  Shortcuts[2] = ShortC;

  Rcpp::List finalList(3);
  finalList[0] = final;
  finalList[1] = Order;
  finalList[2] = Shortcuts;

  return (finalList);
}
