// // [[Rcpp::depends(RcppParallel)]]
// // [[Rcpp::plugins(cpp11)]]
// 
// #include <iostream>
// #include <vector>
// #include <map>
// #include <algorithm>
// #include <stack>
// #include <RcppParallel.h>
// #include <Rcpp.h>
// 
// using namespace RcppParallel;
// using namespace std;
// 
// struct isIndex {
//   
//   int index;
//   
//   isIndex(int idx) : index(idx) {};
//   
//   bool operator()(const int i) {
//     return i == index;
//   }
// };
// 
// inline std::vector<int> get_shortcut_indices (int index, std::vector<int> &shortcut_indices) {
//   
//   std::vector<int> result;
//   
//   std::vector<int>::iterator iter = shortcut_indices.begin();
//   while ((iter= std::find_if(iter, shortcut_indices.end(), isIndex(index))) != shortcut_indices.end()) {
//     
//     result.push_back(std::distance(shortcut_indices.begin(), iter));
//     iter++;
//   }
//   
//   return result;
// }
// 
// inline int intersection(std::vector<int> &indices_0, std::vector<int> &indices_1) {
//   
//   std::vector<int> result;
//   
//   if (indices_0.size() < indices_1.size()) {
//     
//     std::unordered_set<int> u_set(indices_0.begin(), indices_0.end());
//     for (auto i : indices_1) {
//       if (u_set.count(i)) {
//         result.push_back(i);
//         u_set.erase(i);
//       }
//     }
//   }
//   else {
//     
//     std::unordered_set<int> u_set(indices_1.begin(), indices_1.end());
//     for (auto i : indices_0) {
//       if (u_set.count(i)) {
//         result.push_back(i);
//         u_set.erase(i);
//       }
//     }
//   }
//   
//   if (result.size() == 1) return result[0];
//   else return -1;
// }
// 
// inline bool node_in_set(int node, std::vector<int> node_set) {
//   return std::find(node_set.begin(), node_set.end(), node) != node_set.end();
// }
// 
// struct BreakdownShortcut : public Worker {
// 
//   // Input
//   const RcppParallel::RVector<int> shortcut_ids;
//   const RcppParallel::RVector<int> shortcut_nodes;
//   const RcppParallel::RVector<int> from;
//   const RcppParallel::RVector<int> to;
//   const RcppParallel::RVector<int> ft_shortcut;
// 
//   // Output
//   RcppParallel:: final_paths;
// 
//   BreakdownShortcut(
// 
//   )
// };
// 
// // [[Rcpp::export]]
// std::vector<int> reconstruct_path(
//     std::vector<int> &shortcut_ids,
//     std::vector<int> &shortcut_nodes,
//     std::vector<int> &from,
//     std::vector<int> &to,
//     std::vector<int> &ft_shortcut
// ) {
//   
//   // Output
//   std::vector<int> finalPaths;
//   
//   // Store vector indices as maps
//   std::map<int, std::vector<int> > from_idx;
//   std::map<int, std::vector<int> > to_idx;
//   for (auto i: shortcut_ids) {
//     
//     if (node_in_set(i, shortcut_nodes)) {
//       
//       from_idx.insert(make_pair(i, get_shortcut_indices(i, from)));
//       to_idx.insert(make_pair(i, get_shortcut_indices(i, to)));
//       
//       cout << i << endl;
//       for (auto j : from_idx[i]) {
//         cout << j << ", ";
//       }
//       cout << endl;
//     }
//   }
//   
//   for (int i=0; i < shortcut_ids.size()-1; i++) {
//     
//     // cout << i << endl;
//     
//     std:vector<int> holding;
//     
//     std::stack<int> Q;
//     Q.push(shortcut_ids[i+1]);
//     
//     int compare_idx;
//     int shortcut_node;
//     int common_idx;
//     
//     int current_idx = shortcut_ids[i];
//     holding.push_back(current_idx);
//     
//     do {
//       
//       compare_idx = Q.top();
//       Q.pop();
//       
//       if (node_in_set(current_idx, shortcut_nodes) & node_in_set(compare_idx, shortcut_nodes)) {
//         
//         common_idx = intersection(from_idx[current_idx], to_idx[compare_idx]);
//         
//         if (common_idx != -1) {
//           
//           shortcut_node = ft_shortcut[common_idx];
//           
//           Q.push(compare_idx);
//           Q.push(shortcut_node);
//           
//           if (node_in_set(shortcut_node, shortcut_nodes)) {
//             
//             if (from_idx.count(shortcut_node) <= 0) {
//               from_idx.insert(make_pair(shortcut_node, get_shortcut_indices(shortcut_node, from)));
//             }
//             if (to_idx.count(shortcut_node) <= 0) {
//               to_idx.insert(make_pair(shortcut_node, get_shortcut_indices(shortcut_node, to)));
//             }
//           }
//         }
//         else {
//           holding.push_back(compare_idx);
//           current_idx = compare_idx;
//         }
//       }
//       else {
//         holding.push_back(compare_idx);
//         current_idx = compare_idx;
//       }
//     } while (!Q.empty());
//     
//     for (int h : holding) {
//       finalPaths.push_back(h);
//     }
//   }
//   
//   return finalPaths;
// }