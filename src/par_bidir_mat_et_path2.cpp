// [[Rcpp::depends(RcppParallel)]]
// [[Rcpp::plugins(cpp11)]]

#include <iostream>
#include <queue>
#include <vector>
#include <fstream>
#include <limits>
#include <functional>
#include <Rcpp.h>
#include <string>
#include <RcppParallel.h>
#include <Rcpp.h>
using namespace RcppParallel;

struct ParBidirMatSucLin : public Worker {
  
  const RcppParallel::RVector<int> m_NodeGr;
  const RcppParallel::RVector<double> m_WGr;
  const RcppParallel::RVector<int> m_IndGr;
  const RcppParallel::RVector<int> m_NodeBu;
  const RcppParallel::RVector<double> m_WBu;
  const RcppParallel::RVector<int> m_IndBu;
  const RcppParallel::RVector<int> m_Arr;
  const int m_Size;
  const int m_Nb;
  
  //output
  RcppParallel::RMatrix<double> m_result;
  RcppParallel::RMatrix<double> m_successors;
  RcppParallel::RMatrix<double> m_linkers;
  
  //constructor
  ParBidirMatSucLin(
    const Rcpp::IntegerVector &NodeGr,
    const Rcpp::NumericVector &WGr,
    const Rcpp::IntegerVector &IndGr,
    const Rcpp::IntegerVector &NodeBu,
    const Rcpp::NumericVector &WBu,
    const Rcpp::IntegerVector &IndBu,
    const Rcpp::IntegerVector &Arr,
    const int &Size,
    const int &Nb,
    Rcpp::NumericMatrix Result,
    Rcpp::NumericMatrix Successors,
    Rcpp::NumericMatrix Linkers) 
    : m_NodeGr(NodeGr), m_WGr(WGr), m_IndGr(IndGr),
      m_NodeBu(NodeBu), m_WBu(WBu), m_IndBu(IndBu),
      m_Arr(Arr), 
      m_Size(Size), m_Nb(Nb), 
      m_result(Result), m_successors(Successors), m_linkers(Linkers) {}
  
  //overload () operator
  void operator()(std::size_t begin, std::size_t end) {
    
    struct comp{
      
      bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b) {
        return a.second > b.second;
      }
    };
    
    std::vector<double> Distances(m_Nb, std::numeric_limits<double>::max());
    
    for (std::size_t k=begin; k!=end; k++) {
      
      std::vector<double> Dist(m_Size, std::numeric_limits<double>::max());
      
      int StartNode = m_Arr[k];
      Distances[StartNode] = 0.0;
      
      std::priority_queue<std::pair<int, double>, std::vector<std::pair<int, double> >, comp> Q;
      Q.push(std::make_pair(StartNode, 0.0)); 
      
      while (true) {  
        
        if (Q.empty()) {
          break;
        }  
        
        if (!Q.empty()) {
          
          int v = Q.top().first;
          double w = Q.top().second;
          Q.pop();
          
          //Scan bucket
          if ((m_IndBu[v+1] - m_IndBu[v]) > 0) {  // True only if v has been visited
            
            for (int i=m_IndBu[v]; i < m_IndBu[v+1]; i++) {
              
              int Source = m_NodeBu[i];
              double D = Distances[v] + m_WBu[i];  // m_WBu[i]: source[i] to v, Distances[v]: v to arr[k]
              
              if (D < Dist[Source]) {
                
                Dist[Source] = D;
                
                m_linkers(Source, k) = v;
              }
            }
          }
          
          if (w <= Distances[v]) { // Either first or best 
            for (int i=m_IndGr[v]; i < m_IndGr[v+1]; i++) {
              
              int v2 = m_NodeGr[i];                                                     
              double w2 = m_WGr[i];
              
              if (Distances[v] + w2 < Distances[v2]) { // Either first or best 
                
                Distances[v2] = Distances[v] + w2;                                   
                Q.push(std::make_pair(v2, Distances[v2]));
                
                m_successors(k, v2) = v;
              }
            }
          }
        }
      }
      
      std::fill(Distances.begin(), Distances.end(), std::numeric_limits<double>::max());
      
      for (unsigned int i=0; i < Dist.size(); i++) {
        
        if (Dist[i] == std::numeric_limits<double>::max()) Dist[i] = Rcpp::NumericVector::get_na();
        
        m_result(k,i) = Dist[i];
      }
    }
  }
};

// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::export]]
Rcpp::List par_Bidir_mat_et_path2(
    std::vector<int> &dep, 
    std::vector<int> &arr,
    std::vector<int> &gfrom,
    std::vector<int> &gto,
    std::vector<double> &gw,
    int NbNodes,
    std::vector<int> &Rank) {
  
  // BucketF[i].push_back(std::make_pair(k, Distances[i]))
  // Store the distance of going to i from k
  std::vector<std::vector<std::pair<int,double> > > BucketF(NbNodes);
  
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b) {
      return a.second > b.second;
    }
  };
  
  //Graphs
  int NbEdges = gfrom.size();
  
  std::vector<std::vector<std::pair<int, double> > > G(NbNodes);   
  std::vector<std::vector<std::pair<int, double> > > Gr(NbNodes);
  
  int count = 0;
  for (int i = 0; i < NbEdges; ++i) {
    
    if (Rank[gfrom[i]] < Rank[gto[i]]) G[gfrom[i]].push_back(std::make_pair(gto[i], gw[i]));
    if (Rank[gfrom[i]] > Rank[gto[i]]) {
      Gr[gto[i]].push_back(std::make_pair(gfrom[i], gw[i]));
      count += 1;
    }
  }
  
  // Forward
  std::vector<double> Distances(NbNodes, std::numeric_limits<double>::max()); 
  std::vector <int> Visited(NbNodes, 0);
  
  std::vector <std::vector <int> > Predecessors(dep.size(), std::vector <int> (NbNodes, 0));  // Try storing predecessors as vectors for now
  
  for (unsigned int k=0; k!=dep.size(); k++) {
    
    if (k % 256) {
      Rcpp::checkUserInterrupt();
    }
    
    int StartNode = dep[k];
    Distances[StartNode] = 0.0;  
    
    std::priority_queue<std::pair<int, double>, std::vector<std::pair<int, double> >, comp> Q;
    Q.push(std::make_pair(StartNode, 0.0)); 
    
    while (true) {  
      
      if (Q.empty()) {
        break;
      }
      else {
        
        int v = Q.top().first;
        double w = Q.top().second;
        Q.pop();
        
        Visited[v] = 1;
        
        if (w <= Distances[v]) {
          for (unsigned int i=0; i< G[v].size(); i++) {
            
            int v2 = G[v][i].first;                                                     
            double w2 = G[v][i].second;
            
            if (Distances[v] + w2 < Distances[v2]) {                               
              Distances[v2] = Distances[v] + w2;                                   
              Q.push(std::make_pair(v2, Distances[v2]));
              Visited[v2] = 1;
              
              Predecessors[k][v2] = v;
            }
          }
        }
      }
    }
    
    for (unsigned int i=0; i < Distances.size(); i++) {
      if (Distances[i]<std::numeric_limits<double>::max()) {
        BucketF[i].push_back(std::make_pair(k, Distances[i]));
      }
    }
    
    std::fill(Distances.begin(), Distances.end(), std::numeric_limits<double>::max());
  }
  
  int count2 = 0;
  for (unsigned int i=0; i < BucketF.size(); i++) {
    count2 += BucketF[i].size();
    std::sort(BucketF[i].begin(), BucketF[i].end());
  }
  
  std::vector<std::vector<std::pair<int, double> > > ().swap(G);
  
  // Prepare data for backward search
  // Graph vectors
  Rcpp::IntegerVector NodeGr(count);  // count = # edges in reverse graph
  Rcpp::NumericVector WGr(count);
  Rcpp::IntegerVector IndGr(NbNodes+1);
  
  count = 0;
  for (unsigned int i=0; i < Gr.size(); i++) {
    
    IndGr[i] = count; // Contains the index at which the node appears, IndGr[node] = index
    
    for (unsigned int j=0; j < Gr[i].size(); j++) {
      NodeGr[count] = Gr[i][j].first;
      WGr[count] = Gr[i][j].second;
      count += 1;
    }
    // Implies (NodeGr, WGr) is the flattened form of Gr with IndGr as indexer
  }
  
  IndGr[NbNodes] = count;
  std::vector<std::vector<std::pair<int, double> > > ().swap(Gr);  // Free memory
  
  // Bucket vectors
  Rcpp::IntegerVector NodeBu(count2);
  Rcpp::NumericVector WBu(count2);
  Rcpp::IntegerVector IndBu(NbNodes+1);
  count = 0;
  for (unsigned int i=0; i < BucketF.size(); i++) {
    
    IndBu[i] = count;
    
    for (unsigned int j=0; j < BucketF[i].size(); j++) {
      NodeBu[count] = BucketF[i][j].first;  // Contains ORIGIN nodes
      WBu[count] = BucketF[i][j].second;
      count += 1;
    }
  }
  
  IndBu[NbNodes] = count;
  std::vector<std::vector<std::pair<int, double> > > ().swap(BucketF);  // Free memory
  
  // Output objects
  Rcpp::NumericMatrix result(arr.size(), dep.size());
  Rcpp::NumericMatrix Successors(arr.size(), NbNodes);
  Rcpp::NumericMatrix Linkers(dep.size(), arr.size());
  
  ParBidirMatSucLin Dijfunc(
      NodeGr, WGr, IndGr,
      NodeBu, WBu, IndBu,
      Rcpp::wrap(arr), 
      dep.size(), NbNodes, 
      result, Successors, Linkers);
  
  // start, end, function object with overrided operator
  parallelFor(0, arr.size(), Dijfunc);
  
  // Get (shortcut) paths
  Rcpp::List shortcut_paths(dep.size());
  for (unsigned int d=0; d<dep.size(); d++) {
    
    Rcpp::List shortcut_paths_d(arr.size());
    for (unsigned int a=0; a<arr.size(); a++) {
      
      if (Linkers(d, a) == 0) continue;
      
      // Successors : arr.size() x NbNodes
      // Predecessors : dep.size() x NbNodes
      
      // std::cout << "Finding path for " << d << " " << a << std::endl;
      // std::cout << "Finding path for " << dep[d] << " " << arr[a] << " " << Linkers(d, a) << std::endl;
      
      std::vector<int> shortcut_path;
      
      int next_v = Linkers(d, a);
      while (next_v != 0) {
        
        shortcut_path.push_back(next_v);
        next_v = Predecessors[d][next_v];
      }
      
      std::reverse(shortcut_path.begin(), shortcut_path.end());
      
      // std::vector<int> backward_path;
      next_v = Linkers(d, a);
      while (next_v != 0) {
        
        if (next_v != Linkers(d, a)) shortcut_path.push_back(next_v);
        next_v = Successors(a, next_v);
      }
      
      shortcut_paths_d[a] = shortcut_path;
    }
    
    shortcut_paths[d] = shortcut_paths_d;
  }
  
  Rcpp::List return_list(2);
  
  return_list[0] = Rcpp::transpose(result);
  return_list[1] = shortcut_paths;
  
  return return_list;
}


