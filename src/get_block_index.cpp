#include<vector>
#include<cmath>

// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::export]]
std::vector<int> get_block_index (
  std::vector<double> &lats, std::vector<double> &longs, 
  std::vector<double> &block_lats, std::vector<double> &block_longs) {
  
  double dist;
  double min_dist;
  int min_idx;
  
  std::vector<int> indices(lats.size(), -1);
  // std::vector<double> min_dists(lats.size(), 9999999999.9);
  for (unsigned int i=0; i < lats.size(); i++) {
    
    min_dist = 9999999999.9;
    min_idx = -1;
    
    for (unsigned int j=0; j < block_lats.size(); j++) {
      
      dist = std::pow(lats[i] - block_lats[j], 2) + std::pow(longs[i] - block_longs[j], 2);
      if (dist < min_dist) {
        min_dist = dist;
        min_idx = j+1;
      }
    }
    
    indices[i] = min_idx;
  }
  
  // Rcpp::List result(2);
  // result[0] = indices;
  // result[1] = min_dists;
  
  return indices;
}
