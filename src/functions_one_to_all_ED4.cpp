#include <iostream>
#include <queue>
#include <vector>
#include <fstream>
#include <limits>
#include <functional>
#include <Rcpp.h>
#include <chrono>
#include <algorithm>


using namespace std;

// [[Rcpp::plugins(cpp11)]]

void quickDelete3(int idx, std::vector<std::pair<int, double> > &vec) {
  vec[idx] = vec.back();
  vec.pop_back();
}

void quickDelete4(int idx, std::vector<std::pair<int, std::vector<double> > > &vec) {
  vec[idx] = vec.back();
  vec.pop_back();
}

void Dijkstra_mod2(
    std::vector<std::vector<std::pair<int, double> > > &Graph,
    std::vector<std::vector<std::pair<int, double> > > &OrGraph,
    int dep, 
    std::vector<int> &arr, std::vector<double> &lim, 
    int NbNodes, 
    std::vector<double> &Distances) {
  
  // Modifies Graph to exclude wrong shortcuts
  // Uses OrGraph as original reference only
  
  double maxlim = *max_element(lim.begin(), lim.end());
  
  // Comparator 
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b){
      return a.second > b.second;
    }
  };
  
  // Dijkstra
  Distances[dep] = 0.0;                                                     
  
  priority_queue<std::pair<int, double>, vector<std::pair<int, double> >, comp> Q;
  Q.push(std::make_pair(dep, 0.0));   
  
  std::vector<int> visited;
  while (!Q.empty()) {
    
    int v = Q.top().first;                                                     
    double w = Q.top().second;
    
    Q.pop();
    visited.push_back(v);
    
    // Here we consider only those nodes whose distances are less than the span created by
    // the immediate successors of dep
    if (Distances[v] > maxlim) {
      break;
    }
    
    if (w <= Distances[v]) {                                                    
      
      for (unsigned int i=0; i < OrGraph[v].size(); i++) {
        
        std::pair<int,double> j = OrGraph[v][i];                                               
        int v2 = j.first;                                                      
        double w2 = j.second;
        
        if (Distances[v] + w2 < Distances[v2]) { 
          
          Distances[v2] = Distances[v] + w2;    
          
          Q.push(make_pair(v2, Distances[v2]));
          visited.push_back(v2);
        }
      }
    }
  }
  
  
  for (unsigned int i=0; i < arr.size(); i++) {
    
    // This only happens if d(dep -> arr[i]) > d(dep -> some subpath -> arr[i])
    // Therefore, delete dep -> arr[i]
    if (Distances[arr[i]] < lim[i]) {
      
      for (unsigned int j=0; j < Graph[dep].size(); j++) {
        
        if (Graph[dep][j].first == arr[i]) {
          quickDelete3(j, Graph[dep]);
          break;
        }
      }
    }
  }
  
  // Reinitialize distances vector
  for (unsigned int i=0; i < visited.size(); i++) {
    Distances[visited[i]] = std::numeric_limits<double>::max();
  }
}

// TODO: move to own file
template <typename T> inline double simple_objective(T x) {
  return x[0];
}

void Dijkstra_mod2_VOV(
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graph,
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &OrGraph,
    int dep, 
    std::vector<int> &arr, std::vector<std::vector<double> > &lim, 
    int NbNodes, 
    std::vector<double> &Distances) {
  
  // Modifies Graph to exclude wrong shortcuts
  // Uses OrGraph as original reference only
  
  // Comparator for vectors with simple objective function 
  struct simple_comp{
    
    bool operator()(const std::vector<double> &a, std::vector<double> &b) {
      return a[0] < b[0];
    }
  } simple_comparator;
  // TODO: Make complex
  double maxlim = (*max_element(lim.begin(), lim.end(), simple_comparator))[0];
  
  // Comparator for edge importance, therefore, do not touch!
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b){
      return a.second > b.second;
    }
  };
  
  // Dijkstra
  Distances[dep] = 0.0;  // Stores (calculated) objective values
  
  priority_queue<std::pair<int, double>, vector<std::pair<int, double> >, comp> Q;  // Stores index, and (calculated) objective value
  Q.push(std::make_pair(dep, 0.0));   
  
  std::vector<int> visited;
  while (!Q.empty()) {
    
    int v = Q.top().first;                                                     
    double w = Q.top().second;

    Q.pop();
    visited.push_back(v);
    
    // Here we consider only those nodes whose distances are less than the span created by
    // the immediate successors of dep
    if (Distances[v] > maxlim) {
      break;
    }
    
    if (w <= Distances[v]) {                                                    
      
      for (unsigned int i=0; i < OrGraph[v].size(); i++) {
        
        std::pair<int, std::vector<double> > j = OrGraph[v][i];                                               
        int v2 = j.first;                                                      
        std::vector<double> w2 = j.second;
        
        // TODO: Remove repeated calculations
        if (Distances[v] + simple_objective(w2) < Distances[v2]) { 
          
          Distances[v2] = Distances[v] + simple_objective(w2);    
          
          Q.push(make_pair(v2, Distances[v2]));
          visited.push_back(v2);
        }
      }
    }
  }
  
  
  for (unsigned int i=0; i < arr.size(); i++) {
    
    // This only happens if d(dep -> arr[i]) > d(dep -> some subpath -> arr[i])
    // Therefore, delete dep -> arr[i]
    if (Distances[arr[i]] < simple_objective(lim[i])) {
      
      for (unsigned int j=0; j < Graph[dep].size(); j++) {
        
        if (Graph[dep][j].first == arr[i]) {
          quickDelete4(j, Graph[dep]);
          break;
        }
      }
    }
  }
  
  // Reinitialize distances vector
  for (unsigned int i=0; i < visited.size(); i++) {
    Distances[visited[i]] = std::numeric_limits<double>::max();
  }
}

int Dijkstra_bool(
    std::vector<std::vector<std::pair<int, double> > > &Graph,
    int dep, std::vector<int> &arr, 
    std::vector<double> &lim, 
    int NbNodes, 
    int node, 
    std::vector<double> &Distances) {
  
  double maxlim = *max_element(lim.begin(), lim.end());
  
  // Comparator 
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b) {
      return a.second > b.second;
    }
  };
  
  // Dijkstra
  Distances[dep] = 0.0;
  
  priority_queue<std::pair<int, double>, vector<std::pair<int, double> >, comp> Q;
  Q.push(std::make_pair(dep, 0.0));
  
  std::vector<int> visited;
  while (!Q.empty()) {
    
    int v = Q.top().first;                                                     
    double w = Q.top().second;  
    
    Q.pop();
    visited.push_back(v);
    
    if (v == node){ // node to be contracted
      continue;
    }
    if (Distances[v] > maxlim){
      break;
    }
    
    if (w <= Distances[v]) {                                                    
      
      for (unsigned int i=0; i < Graph[v].size(); i++) {
        
        std::pair<int, double> j = Graph[v][i];                                               
        int v2 = j.first;                                                      
        double w2 = j.second;
        
        if (v2 == node) { //node to be contracted, 
          continue;
        }
        
        if (Distances[v] + w2 < Distances[v2]) {   
          
          Distances[v2] = Distances[v] + w2;                                   
          
          Q.push(make_pair(v2, Distances[v2]));
          visited.push_back(v2);
        }
      }
    }
  }
  
  // total gets incremented by one whenever d(dep -> node -> arr[i]) < d(dep -> some other node -> arr[i]) 
  // That is, it scores the number of times node is a useful shortcut in the context of dep and arr
  int total = 0;
  for (unsigned int i=0; i < arr.size(); i++) {
    if (Distances[arr[i]] > lim[i]) total += 1;
  }
  
  //Reinitialize distances vector
  for (unsigned int i=0; i < visited.size(); i++) {
    Distances[visited[i]] = std::numeric_limits<double>::max();
  }
  
  return total;
}

int Edge_dif(
    int node,
    std::vector<std::vector<std::pair<int, double> > > &Graph,
    std::vector<std::vector<std::pair<int, double> > > &Graphr,
    int NbNodes,std::vector<double> &Distances) {
  
  //Node degree
  int outgoing = Graph[node].size();
  int incoming = Graphr[node].size();
  
  if (incoming == 0 || outgoing == 0) return 0 - incoming - outgoing;
  
  if (incoming <= outgoing) {
    
    int shortcuts = 0;
    for (int i=0; i < incoming; i++) {
      
      int dep = Graphr[node][i].first;
      
      double distance_1 = Graphr[node][i].second;
      
      std::vector<double> lim(outgoing, 0.0);
      std::vector<int> arr(outgoing, 0);
      
      // lim[j] contains d(incoming_i -> node -> outgoing[j])
      for (int j=0; j < outgoing; j++) {
        double distance_2 = Graph[node][j].second;
        arr[j] = Graph[node][j].first;
        lim[j] = distance_1 + distance_2;
      }
      
      shortcuts += Dijkstra_bool(Graph, dep, arr, lim, NbNodes, node, Distances);
    }
    
    return shortcuts - incoming - outgoing;
  }
  else {
    
    int shortcuts = 0;
    for (int i=0; i < outgoing; i++) {
      
      int dep = Graph[node][i].first;
      
      double distance_1 = Graph[node][i].second;
      
      std::vector<double> lim(incoming, 0.0);
      std::vector<int> arr(incoming, 0);
      
      // lim[j] contains d(outgoing_i -> node -> incoming[j])
      for (int j=0; j < incoming; j++){
        double distance_2 = Graphr[node][j].second;
        arr[j] = Graphr[node][j].first;
        lim[j] = distance_1 + distance_2;
      }
      
      shortcuts += Dijkstra_bool(Graphr, dep, arr, lim, NbNodes, node, Distances);
    }
    
    return shortcuts - incoming - outgoing;
  }
}

struct min_functor {
  
  double operator()(const double &a, const double &b) {
    return std::min(a, b);
  }
} min_op;

struct max_functor {
  
  double operator()(const double &a, const double &b) {
    return std::max(a, b);
  }
} max_op;

struct avg_functor {
  
  double operator()(const double &a, const double &b) {
    return (a + b) / 2;
  }
} avg_op;

std::vector<double> simplify_metrics(std::vector<double> &a, std::vector<double> &b) {
  
  std::vector<double> zeros(a.size(), 0.0);
  std::transform (a.begin(), a.begin() + 2, b.begin(), zeros.begin(), std::plus<double>());
  std::transform (a.begin() + 2, a.begin() + 4, b.begin() + 2, zeros.begin() + 2, avg_op);
  std::transform (a.begin() + 4, a.begin() + 5, b.begin() + 4, zeros.begin() + 4, min_op);
  std::transform (a.begin() + 5, a.begin() + 15, b.begin() + 5, zeros.begin() + 5, max_op);
  std::transform (a.begin() + 15, a.end(), b.begin() + 15, zeros.begin() + 15, std::plus<double>());
  
  return zeros;
}

int Dijkstra_bool_VOV(
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graph,
    int dep, std::vector<int> &arr, 
    std::vector<double> &lim, 
    int NbNodes, 
    int node, 
    std::vector<double> &Distances) {
  
  double maxlim = *max_element(lim.begin(), lim.end());
  
  // Comparator 
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b) {
      return a.second > b.second;
    }
  };
  
  // Dijkstra
  Distances[dep] = 0.0;
  
  priority_queue<std::pair<int, double>, vector<std::pair<int, double> >, comp> Q;
  Q.push(std::make_pair(dep, 0.0));
  
  std::vector<int> visited;
  while (!Q.empty()) {
    
    int v = Q.top().first;                                                     
    double w = Q.top().second;  
    
    Q.pop();
    visited.push_back(v);
    
    if (v == node){ // node to be contracted
      continue;
    }
    if (Distances[v] > maxlim){
      break;
    }
    
    if (w <= Distances[v]) {                                                    
      
      for (unsigned int i=0; i < Graph[v].size(); i++) {
        
        std::pair<int, std::vector<double> > j = Graph[v][i];                                               
        int v2 = j.first;                                                      
        std::vector<double> w2 = j.second;
        
        if (v2 == node) { //node to be contracted, 
          continue;
        }
        
        if (Distances[v] + simple_objective(w2) < Distances[v2]) {   
          
          Distances[v2] = Distances[v] + simple_objective(w2);                                   
          
          Q.push(make_pair(v2, Distances[v2]));
          visited.push_back(v2);
        }
      }
    }
  }
  
  // total gets incremented by one whenever d(dep -> node -> arr[i]) < d(dep -> some other node -> arr[i]) 
  // That is, it scores the number of times node is a useful shortcut in the context of dep and arr
  int total = 0;
  for (unsigned int i=0; i < arr.size(); i++) {
    if (Distances[arr[i]] > lim[i]) total += 1;
  }
  
  //Reinitialize distances vector
  for (unsigned int i=0; i < visited.size(); i++) {
    Distances[visited[i]] = std::numeric_limits<double>::max();
  }
  
  return total;
}

int Edge_dif_VOV(
    int node,
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graph,
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graphr,
    int NbNodes,std::vector<double> &Distances) {
  
  //Node degree
  int outgoing = Graph[node].size();
  int incoming = Graphr[node].size();
  
  if (incoming == 0 || outgoing == 0) return 0 - incoming - outgoing;
    
  if (incoming <= outgoing) {
    
    int shortcuts = 0;
    for (int i=0; i < incoming; i++) {
      
      int dep = Graphr[node][i].first;
      
      std::vector<double>  distance_1 = Graphr[node][i].second;
      
      std::vector<double> lim(outgoing, 0.0);
      std::vector<int> arr(outgoing, 0);
      
      // lim[j] contains d(incoming_i -> node -> outgoing[j])
      for (int j=0; j < outgoing; j++) {
        std::vector<double> distance_2 = Graph[node][j].second;
        arr[j] = Graph[node][j].first;
        lim[j] = simple_objective(simplify_metrics(distance_1, distance_2));
      }
      
      shortcuts += Dijkstra_bool_VOV(Graph, dep, arr, lim, NbNodes, node, Distances);
    }
    
    return shortcuts - incoming - outgoing;
  }
  else {
    
    int shortcuts = 0;
    for (int i=0; i < outgoing; i++) {
      
      int dep = Graph[node][i].first;
      
      std::vector<double> distance_1 = Graph[node][i].second;
      
      std::vector<double> lim(incoming, 0.0);
      std::vector<int> arr(incoming, 0);
      
      // lim[j] contains d(outgoing_i -> node -> incoming[j])
      for (int j=0; j < incoming; j++){
        std::vector<double> distance_2 = Graphr[node][j].second;
        arr[j] = Graphr[node][j].first;
        lim[j] = simple_objective(simplify_metrics(distance_1, distance_2));
      }
      
      shortcuts += Dijkstra_bool_VOV(Graphr, dep, arr, lim, NbNodes, node, Distances);
    }
    
    return shortcuts - incoming - outgoing;
  }
}

std::vector<std::pair<int,std::pair<int,double> > >  Dijkstra(
    std::vector<std::vector<std::pair<int, double> > > &Graph, // modifiable
    std::vector<std::vector<std::pair<int, double> > > &Graphr, // modifiable
    int dep,
    std::vector<int> &arr,
    std::vector<double> &lim,
    int NbNodes,
    int node,
    std::vector<double> &Distances,
    std::vector<int> &Contracted,
    bool reversed,
    bool &err) {
  
  double maxlim = *max_element(lim.begin(), lim.end());
  
  // Comparator 
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b){
      return a.second > b.second;
    }
  };
  
  Distances[dep] = 0.0;
  
  priority_queue<std::pair<int, double>, vector<std::pair<int, double> >, comp> Q;
  Q.push(std::make_pair(dep, 0.0));   
  
  std::vector<int> visited;
  while (!Q.empty()) {    
    
    int v = Q.top().first;                                                     
    double w = Q.top().second;     
    
    Q.pop();
    visited.push_back(v);
    
    if (v == node){ // node to be contracted, 
      continue;
    }
    if (Distances[v] > maxlim){
      break;
    }
    
    if (w <= Distances[v]) {                                                    
      
      for (unsigned int i=0; i < Graph[v].size(); i++) {
        
        std::pair<int, double> j = Graph[v][i];                                               
        int v2 = j.first;                                                      
        double w2 = j.second;
        
        if (v2 == node){ // node to be contracted, 
          continue;
        }
        
        if (Distances[v] + w2 < Distances[v2]) {   
          
          Distances[v2] = Distances[v] + w2;                                   
          
          Q.push(make_pair(v2, Distances[v2]));
          visited.push_back(v2);
        }
      }
    }
  }
  
  // Short is a list of valid (from, to, shortcut distance)
  // Valid in the sense that Distances[arr[i]] > lim[i]
  // Which happens whenever from -> to yields the local minimum distance
  std::vector<std::pair<int, std::pair<int, double> > > Short;
  for (unsigned int i=0; i < arr.size(); i++) {
    
    if (Distances[arr[i]] > lim[i]) {
      
      if (dep == arr[i]) continue;
      
      if (reversed == true){
        
        Short.push_back(std::make_pair(arr[i], std::make_pair(dep, lim[i])));
      }
      else {
        
        Short.push_back(std::make_pair(dep, std::make_pair(arr[i], lim[i])));
      }
    }
  }
  
  //Reinitialize distances vector
  for (unsigned int i=0; i < visited.size(); i++) {
    Distances[visited[i]] = std::numeric_limits<double>::max();
  }
  
  return Short;
}

void Contract(
    int node,
    std::vector<std::vector<std::pair<int, double> > > &Graph,//modifiable
    std::vector<std::vector<std::pair<int, double> > > &Graphr,//modifiable
    std::vector<std::vector<std::pair<int, double> > > &OrGraph,//augmented (original + shortcuts)
    int NbNodes,
    std::vector<double> &Distances,
    std::vector<int> &Contracted,
    int count,
    bool &err,
    std::vector<int> &ShortF,
    std::vector<int> &ShortT,
    std::vector<int> &ShortC) {
  
  // Node degree
  int outgoing = Graph[node].size();
  int incoming = Graphr[node].size();
  
  std::vector<std::pair<int,std::pair<int,double> > > Final;
  if (incoming == 0 || outgoing == 0) {
    
    Contracted[node] = 1;
  }
  else {
    
    if (incoming <= outgoing) {
      
      for (int i=0; i < incoming; i++) {
        
        int dep = Graphr[node][i].first;
        
        double distance_1 = Graphr[node][i].second;
        
        std::vector<double> lim(outgoing, 0.0);
        std::vector<int> arr(outgoing, 0);
        
        for (int j=0; j < outgoing; j++){
          double distance_2=Graph[node][j].second;
          arr[j] = Graph[node][j].first;
          lim[j] = distance_1 + distance_2;
        }
        
        std::vector<std::pair<int,std::pair<int,double> > > result = Dijkstra(
          Graph, Graphr,
          dep,
          arr, lim,
          NbNodes,
          node,
          Distances, Contracted,
          false, err);
        
        for (unsigned int j=0; j < result.size(); j++) Final.push_back(result[j]);
      }
    }
    else {
      
      for (int i=0; i < outgoing; i++){
        
        int dep = Graph[node][i].first;
        
        double distance_1=Graph[node][i].second;
        
        std::vector<double> lim(incoming, 0.0);
        std::vector<int> arr(incoming, 0);
        
        for (int j=0; j < incoming; j++){
          double distance_2 = Graphr[node][j].second;
          arr[j] = Graphr[node][j].first;
          lim[j] = distance_1 + distance_2;
        }
        
        std::vector<std::pair<int,std::pair<int,double> > > result = Dijkstra(
          Graphr, Graph,
          dep,
          arr, lim,
          NbNodes,
          node,
          Distances, Contracted,
          true, err);
        
        for (unsigned int j=0; j < result.size(); j++) Final.push_back(result[j]);
      }
    }
  }
  
  //Remove all associations to and from contracted node
  Contracted[node] = 1;
  
  for (int j=0; j < incoming; j++) {
    
    std::vector<int> ind;
    int nd = Graphr[node][j].first;
    
    for (unsigned int i=0; i < Graph[nd].size(); i++) {
      if (Graph[nd][i].first == node) ind.push_back(i);
    }
    
    // Reverse because order is shifted in Graph[nd] whenever ind[i] is deleted
    // Therefore delete element with largest index first
    if (ind.size() > 0) std::reverse(ind.begin(), ind.end());
    for (unsigned int i=0; i < ind.size(); i++) quickDelete3(ind[i], Graph[nd]);
  }
  
  for (int j=0; j < outgoing; j++) {
    
    std::vector<int> ind2;
    int nd = Graph[node][j].first;
    
    for (unsigned int i=0; i < Graphr[nd].size(); i++) {
      if (Graphr[nd][i].first == node) ind2.push_back(i);
    }
    
    // Same as above
    if (ind2.size() > 0) std::reverse(ind2.begin(), ind2.end());
    for (unsigned int i=0; i < ind2.size(); i++) quickDelete3(ind2[i], Graphr[nd]);
  }
  
  Graph[node].erase (Graph[node].begin(), Graph[node].end());
  Graphr[node].erase (Graphr[node].begin(), Graphr[node].end());
  
  // Shortcuts
  bool dup;
  for (unsigned int i=0; i < Final.size(); i++) {
    
    //Add shortcut to the final graph
    OrGraph[Final[i].first].push_back(
        std::make_pair(Final[i].second.first, Final[i].second.second));
    
    ShortF.push_back(Final[i].first);
    ShortT.push_back(Final[i].second.first);
    ShortC.push_back(node);
    
    //Duplicated
    dup = false;
    for (unsigned int k=0; k < Graph[Final[i].first].size(); k++) {
      if (Graph[Final[i].first][k].first == Final[i].second.first && Graph[Final[i].first][k].second > Final[i].second.second) {
        Graph[Final[i].first][k].second = Final[i].second.second;
        dup = true;
        break;
      }
    }
    
    if (dup == false) Graph[Final[i].first].push_back(std::make_pair(Final[i].second.first, Final[i].second.second));
    
    dup = false;
    for (unsigned int k=0; k < Graphr[Final[i].second.first].size(); k++) {
      if (Graphr[Final[i].second.first][k].first == Final[i].first && Graphr[Final[i].second.first][k].second > Final[i].second.second) {
        Graphr[Final[i].second.first][k].second = Final[i].second.second;
        dup = true;
        break;
        
      }
    }
    
    if (dup == false) Graphr[Final[i].second.first].push_back(std::make_pair(Final[i].first, Final[i].second.second));
  }
}

std::vector<std::pair<int,std::pair<int, std::vector<double> > > >  Dijkstra_VOV(
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graph, // modifiable
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graphr, // modifiable
    int dep,
    std::vector<int> &arr,
    std::vector<std::vector<double> > &lim,
    int NbNodes,
    int node,
    std::vector<double> &Distances,
    std::vector<int> &Contracted,
    bool reversed,
    bool &err) {
  
  struct simple_comp{
    
    bool operator()(const std::vector<double> &a, std::vector<double> &b) {
      return a[0] < b[0];
    }
  } simple_comparator;
  
  double maxlim = (*max_element(lim.begin(), lim.end(), simple_comparator))[0];
  
  // Comparator 
  struct comp{
    
    bool operator()(const std::pair<int, double> &a, const std::pair<int, double> &b){
      return a.second > b.second;
    }
  };
  
  Distances[dep] = 0.0;
  
  priority_queue<std::pair<int, double>, vector<std::pair<int, double> >, comp> Q;
  Q.push(std::make_pair(dep, 0.0));   
  
  std::vector<int> visited;
  while (!Q.empty()) {    
    
    int v = Q.top().first;                                                     
    double w = Q.top().second;     
    
    Q.pop();
    visited.push_back(v);
    
    if (v == node){ // node to be contracted, 
      continue;
    }
    if (Distances[v] > maxlim){
      break;
    }
    
    if (w <= Distances[v]) {                                                    
      
      for (unsigned int i=0; i < Graph[v].size(); i++) {
        
        std::pair<int, std::vector<double> > j = Graph[v][i];                                               
        int v2 = j.first;                                                      
        std::vector<double> w2 = j.second;
        
        if (v2 == node){ // node to be contracted, 
          continue;
        }
        
        if (Distances[v] + simple_objective(w2) < Distances[v2]) {   
          
          Distances[v2] = Distances[v] + simple_objective(w2);                                   
          
          Q.push(make_pair(v2, Distances[v2]));
          visited.push_back(v2);
        }
      }
    }
  }
  
  // Short is a list of valid (from, to, shortcut distance)
  // Valid in the sense that Distances[arr[i]] > lim[i]
  // Which happens whenever from -> to yields the local minimum distance
  std::vector<std::pair<int, std::pair<int, std::vector<double> > > > Short;
  for (unsigned int i=0; i < arr.size(); i++) {
    
    if (Distances[arr[i]] > simple_objective(lim[i])) {
      
      if (dep == arr[i]) continue;
      
      if (reversed == true){
        
        Short.push_back(std::make_pair(arr[i], std::make_pair(dep, lim[i])));
      }
      else {
        
        Short.push_back(std::make_pair(dep, std::make_pair(arr[i], lim[i])));
      }
    }
  }
  
  //Reinitialize distances vector
  for (unsigned int i=0; i < visited.size(); i++) {
    Distances[visited[i]] = std::numeric_limits<double>::max();
  }
  
  return Short;
}

void Contract_VOV(
    int node,
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graph, //modifiable
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &Graphr, //modifiable
    std::vector<std::vector<std::pair<int, std::vector<double> > > > &OrGraph, //augmented (original + shortcuts)
    int NbNodes,
    std::vector<double> &Distances,
    std::vector<int> &Contracted,
    int count,
    bool &err,
    std::vector<int> &ShortF,
    std::vector<int> &ShortT,
    std::vector<int> &ShortC) {
  
  // Node degree
  int outgoing = Graph[node].size();
  int incoming = Graphr[node].size();
  
  std::vector<std::pair<int,std::pair<int, std::vector<double> > > > Final;
  if (incoming == 0 || outgoing == 0) {
    
    Contracted[node] = 1;
  }
  else {
    
    if (incoming <= outgoing) {
      
      for (int i=0; i < incoming; i++) {
        
        int dep = Graphr[node][i].first;
        
        std::vector<double> distance_1 = Graphr[node][i].second;
        
        std::vector<std::vector<double> > lim;
        std::vector<int> arr(outgoing, 0);
        
        // HERE
        
        for (int j=0; j < outgoing; j++){
          std::vector<double> distance_2 = Graph[node][j].second;
          arr[j] = Graph[node][j].first;
          lim.push_back(simplify_metrics(distance_1, distance_2));
        }
        
        std::vector<std::pair<int,std::pair<int, std::vector<double> > > > result = Dijkstra_VOV(
          Graph, Graphr,
          dep,
          arr, lim,
          NbNodes,
          node,
          Distances, Contracted,
          false, err);
        
        for (unsigned int j=0; j < result.size(); j++) Final.push_back(result[j]);
      }
    }
    else {
      
      for (int i=0; i < outgoing; i++){
        
        int dep = Graph[node][i].first;
        
        std::vector<double> distance_1 = Graph[node][i].second;
         
        std::vector<std::vector<double> > lim;
        std::vector<int> arr(incoming, 0);
        
        for (int j=0; j < incoming; j++){
          std::vector<double> distance_2 = Graphr[node][j].second;
          arr[j] = Graphr[node][j].first;
          lim.push_back(simplify_metrics(distance_1, distance_2));
        }
        
        std::vector<std::pair<int,std::pair<int, std::vector<double> > > > result = Dijkstra_VOV(
          Graphr, Graph,
          dep,
          arr, lim,
          NbNodes,
          node,
          Distances, Contracted,
          true, err);
        
        for (unsigned int j=0; j < result.size(); j++) Final.push_back(result[j]);
      }
    }
  }
  
  // Remove all associations to contracted node
  Contracted[node] = 1;
  
  for (int j=0; j < incoming; j++) {
    
    std::vector<int> ind;
    int nd = Graphr[node][j].first;
    
    for (unsigned int i=0; i < Graph[nd].size(); i++) {
      if (Graph[nd][i].first == node) ind.push_back(i);
    }
    
    // Reverse because order is shifted in Graph[nd] whenever ind[i] is deleted
    // Therefore delete element with largest index first
    if (ind.size() > 0) std::reverse(ind.begin(), ind.end());
    for (unsigned int i=0; i < ind.size(); i++) quickDelete4(ind[i], Graph[nd]);
  }
  
  // Remove all associations from contracted node
  for (int j=0; j < outgoing; j++) {
    
    std::vector<int> ind2;
    int nd = Graph[node][j].first;
    
    for (unsigned int i=0; i < Graphr[nd].size(); i++) {
      if (Graphr[nd][i].first == node) ind2.push_back(i);
    }
    
    // Same as above
    if (ind2.size() > 0) std::reverse(ind2.begin(), ind2.end());
    for (unsigned int i=0; i < ind2.size(); i++) quickDelete4(ind2[i], Graphr[nd]);
  }
  
  Graph[node].erase (Graph[node].begin(), Graph[node].end());
  Graphr[node].erase (Graphr[node].begin(), Graphr[node].end());
  
  // Shortcuts
  bool dup;
  for (unsigned int i=0; i < Final.size(); i++) {
    
    // Add shortcut to the final graph
    OrGraph[Final[i].first].push_back(
        std::make_pair(Final[i].second.first, Final[i].second.second));
    
    ShortF.push_back(Final[i].first);
    ShortT.push_back(Final[i].second.first);
    ShortC.push_back(node);
    
    //Duplicated
    dup = false;
    for (unsigned int k=0; k < Graph[Final[i].first].size(); k++) {
      if (Graph[Final[i].first][k].first == Final[i].second.first && simple_objective(Graph[Final[i].first][k].second) > simple_objective(Final[i].second.second)) {
        Graph[Final[i].first][k].second = Final[i].second.second;
        dup = true;
        break;
      }
    }
    if (dup == false) Graph[Final[i].first].push_back(std::make_pair(Final[i].second.first, Final[i].second.second));
    
    dup = false;
    for (unsigned int k=0; k < Graphr[Final[i].second.first].size(); k++) {
      if (Graphr[Final[i].second.first][k].first == Final[i].first && simple_objective(Graphr[Final[i].second.first][k].second) > simple_objective(Final[i].second.second)) {
        Graphr[Final[i].second.first][k].second = Final[i].second.second;
        dup = true;
        break;
        
      }
    }
    if (dup == false) Graphr[Final[i].second.first].push_back(std::make_pair(Final[i].first, Final[i].second.second));
  }
}
