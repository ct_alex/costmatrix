// [[Rcpp::plugins(cpp11)]]

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <stack>
#include <Rcpp.h>

using namespace std;

inline int intersection(std::vector<int> &indices_0, std::vector<int> &indices_1) {
  
  std::vector<int> result;
  
  if (indices_0.size() < indices_1.size()) {
    
    std::unordered_set<int> u_set(indices_0.begin(), indices_0.end());
    for (auto i : indices_1) {
      if (u_set.count(i)) {
        result.push_back(i);
        u_set.erase(i);
      }
    }
  }
  else {
    
    std::unordered_set<int> u_set(indices_1.begin(), indices_1.end());
    for (auto i : indices_0) {
      if (u_set.count(i)) {
        result.push_back(i);
        u_set.erase(i);
      }
    }
  }
  
  if (result.size() == 1) return result[0];
  else return -1;
}

inline int intersection_v2(vector<int>::iterator it_0, vector<int>::iterator it_1, int length_0, int length_1) {
  
  std::vector<int> result;
  
  if (length_0 < length_1) {
    
    std::unordered_set<int> u_set(it_0, it_0 + length_0);
    
    for (int i=0; i < length_1; i++) {
      if (u_set.count(*(it_1 + i))) {
        
        result.push_back(*(it_1 + i));
        u_set.erase(*(it_1 + i));
      }
    }
  }
  else {
    
    std::unordered_set<int> u_set(it_1, it_1 + length_1);
    for (int i=0; i<length_0; i++) {
      if (u_set.count(*(it_0 + i))) {
        
        result.push_back(*(it_0 + i));
        u_set.erase(*(it_0 + i));
      }
    }
  }
  
  if (result.size() == 1) return result[0];
  else return -1;
}

inline int intersection_v3(
    std::vector<int> &it_0, int start_0, int end_0,
    std::vector<int> &it_1, int start_1, int end_1) {
  
  Rcpp::NumericVector result = Rcpp::intersect(
    Rcpp::NumericVector(it_0.begin() + start_0, it_0.begin() + end_0),
    Rcpp::NumericVector(it_1.begin() + start_1, it_1.begin() + end_1)
  );
  
  if (result.size() == 1) return result[0];
  else return -1;
}

inline bool node_in_set(int node, std::vector<int> node_set) {
  return std::find(node_set.begin(), node_set.end(), node) != node_set.end();
}

// [[Rcpp::export]]
std::vector<int> reconstruct_path_with_shortcut_indices(
    std::vector<int> &shortcut_ids,
    std::vector<int> &from_indices,
    std::vector<int> &from_shortcuts,
    std::vector<int> &to_indices,
    std::vector<int> &to_shortcuts,
    std::vector<int> &ft_shortcut,
    int type,
    int holding_length = 2000,
    int final_holding_length = 10000
) {
  
  int f_idx = 0;
  int h_idx;
  
  std::vector<int> holding(holding_length, 0);
  std::vector<int> holding_final(final_holding_length, 0);
  
  int compare_idx;
  int shortcut_node;
  int common_idx;
  int current_idx;
  
  bool current_has_shortcut;
  bool compare_has_shortcut;
  
  for (unsigned int i=0; i < shortcut_ids.size()-1; i++) {
    
    std::stack<int> Q;
    Q.push(shortcut_ids[i+1]);
    
    current_idx = shortcut_ids[i];
    
    holding[0] = current_idx;
    h_idx = 1;
    
    do {
      
      compare_idx = Q.top();
      Q.pop();
      
      current_has_shortcut = from_indices[current_idx+1] - from_indices[current_idx] > 0;
      compare_has_shortcut = to_indices[compare_idx+1] - to_indices[compare_idx] > 0;
      
      if (current_has_shortcut & compare_has_shortcut) {
        
        if (type == 2) common_idx = intersection_v2(
          from_shortcuts.begin() + from_indices[current_idx],
          to_shortcuts.begin() + to_indices[compare_idx],
          from_indices[current_idx+1] - from_indices[current_idx],
          to_indices[compare_idx+1] - to_indices[compare_idx]);
        
        else common_idx = intersection_v3(
          from_shortcuts, from_indices[current_idx], from_indices[current_idx+1],
          to_shortcuts, to_indices[compare_idx], to_indices[compare_idx+1]);
        
        if (common_idx != -1) {
          
          shortcut_node = ft_shortcut[common_idx];
          
          Q.push(compare_idx);
          Q.push(shortcut_node);
        }
        else {
          holding[h_idx] = compare_idx;
          current_idx = compare_idx;
          
          h_idx += 1;
        }
      }
      else {
        holding[h_idx] = compare_idx;
        current_idx = compare_idx;
        
        h_idx += 1;
      }
    } while (!Q.empty());
    
    for (int h=0; h < h_idx-1; h++) {
      holding_final[f_idx] = holding[h];
      f_idx += 1;
    }
  }
  
  holding_final[f_idx] = holding[h_idx-1];
  f_idx += 1;
  
  // Output
  std::vector<int> result(f_idx);
  for (int i=0; i<f_idx; i++) {
    result[i] = holding_final[i];
  }
  
  return result;
}





































