split_range <- function (range_vals, split_size = 0.5) {
  
  splits <- seq(range_vals[1], range_vals[2], by = split_size)
  if (splits[length(splits)] != range_vals[2]) splits <- c(splits, range_vals[2])
  
  return (splits)
}

get_point_boolean <- function (lats, longs, min_lat, max_lat, min_long, max_long, last_lat, last_long) {
  
  if (last_lat & last_long) {
    
    point_bool <- (
      lats >= min_lat & lats <= max_lat &
        longs >= min_long & longs <= max_long
    )
    
  } else if (last_lat) {
    
    point_bool <- (
      lats >= min_lat & lats <= max_lat &
        longs >= min_long & longs < max_long
    )
    
  } else if (last_long) {
    
    point_bool <- (
      lats >= min_lat & lats < max_lat &
        longs >= min_long & longs <= max_long
    )
    
  } else {
    
    point_bool <- (
      lats >= min_lat & lats < max_lat &
        longs >= min_long & longs < max_long
    )
  }
  
  return (point_bool)
}

get_neighbour_indices <- function (
  centre_lat_idx, centre_long_idx,
  node_total,
  lat_idx_set, long_idx_set,
  nb_idx,
  lat_rest_set, long_rest_set,
  max_nodes_per_block,
  point_bools
) {
  
  if (length(nb_idx) > 0) {
    
    valid_neighbour_idx <- c()
    neighbour_dist <- c()
    for (r_i in 1 : length(lat_rest_set)) {
      
      index_valid <- min(abs(lat_rest_set[r_i] - lat_idx_set) + abs(long_rest_set[r_i] - long_idx_set)) == 1
      node_sum_valid <- node_total + sum(point_bools[[nb_idx[r_i]]]) <= max_nodes_per_block
      
      if (index_valid & node_sum_valid) {
        
        valid_neighbour_idx <- c(valid_neighbour_idx, nb_idx[r_i])
        neighbour_dist <- c(
          neighbour_dist, 
          abs(centre_lat_idx - lat_rest_set[r_i]) + abs(centre_long_idx - long_rest_set[r_i]))
      }
    }
    
    if (length(valid_neighbour_idx) > 0) {
      return (list(
        valid_neighbour_idx = valid_neighbour_idx,
        neighbour_dist = neighbour_dist
      ))
    }
  } 
  
  return (list())
}

get_block_data <- function (
  blocks, block_nodes, block_counter, 
  lat_splits, long_splits, 
  lats, longs, aux_df,
  max_lat, max_long, 
  max_nodes_per_block,
  split_size) {
  
  # split_size <- 2
  # max_nodes_per_block <- 20000
  
  # blocks <- list()
  # block_nodes <- list()
  # block_counter <- 1
  
  # lats <- raw_nodes$lat; longs <- raw_nodes$long
  # max_lat = max(raw_nodes$lat); max_long = max(raw_nodes$long)
  # lat_splits <- range(raw_nodes$lat)
  # long_splits <- range(raw_nodes$long)
  
  # Get total number of combinations
  min_lat_set <- c()
  min_long_set <- c()
  max_lat_set <- c()
  max_long_set <- c()
  point_bools <- list()
  p <- 1
  for (lat_i in 1 : (length(lat_splits) - 1)) {
    for (long_i in 1 : (length(long_splits) - 1)) {
      
      point_bool <- get_point_boolean(
        lats, longs, 
        lat_splits[lat_i], lat_splits[lat_i+1], 
        long_splits[long_i], long_splits[long_i+1], 
        last_lat = lat_splits[lat_i+1] == max_lat, 
        last_long = long_splits[long_i+1] == max_long)
      
      if (any(point_bool)) {
        
        min_lat_set <- c(min_lat_set, lat_i)
        min_long_set <- c(min_long_set, long_i)
        
        max_lat_set <- c(max_lat_set, lat_i+1)
        max_long_set <- c(max_long_set, long_i+1)
        
        point_bools[[p]] <- point_bool
        
        p <- p + 1
      }
    }
  }
  
  k <- 1
  while (length(min_lat_set) > 0) {
    
    print(paste0("ITERATION: ", k))
    
    if (sum(point_bools[[1]]) > max_nodes_per_block) {
      
      # print("Exceeded max_nodes in block")
      block_memory <- Recall(
        blocks, block_nodes, block_counter,
        lat_splits = split_range(
          c(lat_splits[min_lat_set[1]], lat_splits[max_lat_set[1]]), 
          split_size = split_size / 2),
        long_splits = split_range(
          c(long_splits[min_long_set[1]], long_splits[max_long_set[1]]), 
          split_size = split_size / 2),
        lats[point_bools[[1]]], longs[point_bools[[1]]], aux_df[point_bools[[1]],],
        max_lat, max_long,
        max_nodes_per_block = max_nodes_per_block,
        split_size = split_size / 2)
      
      blocks <- block_memory$blocks
      block_nodes <- block_memory$block_nodes
      block_counter <- block_memory$block_counter
      
      min_lat_set <- min_lat_set[-1]
      min_long_set <- min_long_set[-1]
      max_lat_set <- max_lat_set[-1]
      max_long_set <- max_long_set[-1]
      point_bools <- point_bools[-1]
      
    } else {
      
      neighbour_indices <- get_neighbour_indices(
        min_lat_set[1], min_long_set[1],
        sum(point_bools[[1]]),
        min_lat_set[1], min_long_set[1],
        (1 : length(min_lat_set))[-1],
        min_lat_set[-1], min_long_set[-1],
        max_nodes_per_block = max_nodes_per_block,
        point_bools = point_bools
      )
      
      if (length(neighbour_indices) > 0) {
        
        # print("Has valid neighbours")
        # print(neighbour_indices)
        
        set_idx <- c(1)
        while (length(neighbour_indices) > 0) {
          
          best_neighbour_idx <- which(neighbour_indices$neighbour_dist == min(neighbour_indices$neighbour_dist))[1]
          set_idx <- c(set_idx, neighbour_indices$valid_neighbour_idx[best_neighbour_idx])
          
          neighbour_indices <- get_neighbour_indices(
            min_lat_set[1], min_long_set[1],
            sum(unlist(point_bools[set_idx])),
            min_lat_set[set_idx], min_long_set[set_idx],
            (1 : length(min_lat_set))[-set_idx],
            min_lat_set[-set_idx], min_long_set[-set_idx],
            max_nodes_per_block = max_nodes_per_block,
            point_bools = point_bools
          )
        }
        
        # Add block as "set"
        blocks[[block_counter]] <- data.frame(
          min_lat = lat_splits[min_lat_set[set_idx]],
          max_lat = lat_splits[max_lat_set[set_idx]],
          min_long = long_splits[min_long_set[set_idx]],
          max_long = long_splits[max_long_set[set_idx]],
          block_counter = block_counter
        )
        block_nodes[[block_counter]] <- cbind(
          data.frame(
            lat = lats[Reduce("|", point_bools[set_idx])],
            long = longs[Reduce("|", point_bools[set_idx])]
          ),
          aux_df[Reduce("|", point_bools[set_idx]),]
        )
        
        block_counter <- block_counter + 1
        
        # Remove as "set"
        min_lat_set <- min_lat_set[-set_idx]
        min_long_set <- min_long_set[-set_idx]
        max_lat_set <- max_lat_set[-set_idx]
        max_long_set <- max_long_set[-set_idx]
        point_bools <- point_bools[-set_idx]
        
      } else {
        
        # print("No valid neighbours")
        
        # No neighbours to merge
        blocks[[block_counter]] <- data.frame(
          min_lat = lat_splits[min_lat_set[1]],
          max_lat = lat_splits[max_lat_set[1]],
          min_long = long_splits[min_long_set[1]],
          max_long = long_splits[max_long_set[1]],
          block_counter = block_counter
        )
        block_nodes[[block_counter]] <- cbind(
          data.frame(
            lat = lats[point_bools[[1]]],
            long = longs[point_bools[[1]]]
          ),
          aux_df[point_bools[[1]],]
        )
        
        block_counter <- block_counter + 1
        
        min_lat_set <- min_lat_set[-1]
        min_long_set <- min_long_set[-1]
        max_lat_set <- max_lat_set[-1]
        max_long_set <- max_long_set[-1]
        point_bools <- point_bools[-1]
      }
    }
    
    k <- k + 1
  }
  
  return (list(
    blocks = blocks,
    block_nodes = block_nodes,
    block_counter = block_counter
  ))
}

get_graph_references <- function (block_data, lats, longs, max_lat, max_long) {
  
  n <- length(lats)
  
  block_list <- rep(0, n)
  for (b in unique(block_data$blocks$block_counter)) {
    
    for (bb in which(block_data$blocks$block_counter == b)) {
      
      point_bool <- get_point_boolean(
        lats, longs, 
        block_data$blocks$min_lat[bb], block_data$blocks$max_lat[bb], 
        block_data$blocks$min_long[bb], block_data$blocks$max_long[bb], 
        block_data$blocks$max_lat[bb] == max_lat, 
        block_data$blocks$max_long[bb] == max_long)
      
      if (any(point_bool)) {
        
        block_list[which(point_bool)] <- b
      }
    }
  }
  
  graph_ref_df <- data.frame(
    # snap_dist = rep(0, n),
    lat = rep(0, n),
    long = rep(0, n),
    min_ref = rep("0", n),
    max_ref = rep("0", n),
    min_p = rep(0, n),
    minutes = rep(0, n),
    meters = rep(0, n)
  )
  for (b in unique(block_list)) {  # b <- unique(block_list)[1]
    
    if (b > 0) {
      node_idx <- which(block_list == b)
      block_idx <- get_block_index(
        lats[node_idx], longs[node_idx], 
        block_data$block_nodes[[b]]$lat, block_data$block_nodes[[b]]$long)
      
      graph_ref_df[node_idx,] <- block_data$block_nodes[[b]][block_idx,]
    }
  }
  
  return (graph_ref_df)
}
